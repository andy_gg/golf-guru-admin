
import 'package:data_uploader/models/audio.dart';

import 'package:data_uploader/models/profile_type.dart';

class TrackSuggestions {
   String? id;
   String? mgpComponentDescription;
   SuggestionContent? suggestionContent;

  TrackSuggestions({
    this.id,
    this.mgpComponentDescription,
    this.suggestionContent,
  });

  factory TrackSuggestions.fromJson(Map<String, dynamic> json) => TrackSuggestions(
    id: json["_id"],
    mgpComponentDescription: json["mgpComponentDescription"],
    suggestionContent: json["suggestionContent"] == null ? null : SuggestionContent.fromJson(json["suggestionContent"]),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "mgpComponentDescription": mgpComponentDescription,
    "suggestionContent": suggestionContent?.toJson(),
  };
}









class AudioRequirements {
  final int? numberOfBalls;

  AudioRequirements({
    this.numberOfBalls,
  });

  factory AudioRequirements.fromJson(Map<String, dynamic> json) => AudioRequirements(
    numberOfBalls: json["numberOfBalls"],
  );

  Map<String, dynamic> toJson() => {
    "numberOfBalls": numberOfBalls,
  };
}

class Content {
  final String? language;
  final String? gender;
  final String? url;
  final String? previewUrl;
  final int? lengthMins;

  Content({
    this.language,
    this.gender,
    this.url,
    this.previewUrl,
    this.lengthMins,
  });

  factory Content.fromJson(Map<String, dynamic> json) => Content(
    language: json["language"],
    gender: json["gender"],
    url: json["url"],
    previewUrl: json["previewUrl"],
    lengthMins: json["lengthMins"],
  );

  Map<String, dynamic> toJson() => {
    "language": language,
    "gender": gender,
    "url": url,
    "previewUrl": previewUrl,
    "lengthMins": lengthMins,
  };
}

class Description {
  final String? long;
  final String? short;

  Description({
    this.long,
    this.short,
  });

  factory Description.fromJson(Map<String, dynamic> json) => Description(
    long: json["long"],
    short: json["short"],
  );

  Map<String, dynamic> toJson() => {
    "long": long,
    "short": short,
  };
}

class VoicedBy {
  final String? name;
  final Images? image;

  VoicedBy({
    this.name,
    this.image,
  });

  factory VoicedBy.fromJson(Map<String, dynamic> json) => VoicedBy(
    name: json["name"],
    image: json["image"] == null ? null : Images.fromJson(json["image"]),
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "image": image?.toJson(),
  };
}
