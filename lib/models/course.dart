

import 'package:data_uploader/models/audio.dart';
import 'package:data_uploader/models/profile_type.dart';

class Course {
   String? id;
   String? name;
   String? description;
   List<String>? mentalSkills;
   int? difficulty;
   int? numberOfDays;
   Images? headerImage;
   dynamic locationIDs;
   dynamic categoryIDs;
   int? createdAt;
   int? updatedAt;

  Course({
    this.id,
    this.name,
    this.description,
    this.mentalSkills,
    this.difficulty,
    this.numberOfDays,
    this.headerImage,
    this.locationIDs,
    this.categoryIDs,
    this.createdAt,
    this.updatedAt,
  });

  factory Course.fromJson(Map<String, dynamic> json) => Course(
    id: json["_id"],
    name: json["name"],
    description: json["description"],
    mentalSkills: json["mentalSkills"] == null ? [] : List<String>.from(json["mentalSkills"]!.map((x) => x)),
    difficulty: json["difficulty"],
    numberOfDays: json["numberOfDays"],
    headerImage: json["headerImage"] == null ? null : Images.fromJson(json["headerImage"]),
    locationIDs: json["locationIDs"],
    categoryIDs: json["categoryIDs"],
    createdAt: json["createdAt"],
    updatedAt: json["updatedAt"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "description": description,
    "mentalSkills": mentalSkills == null ? [] : List<dynamic>.from(mentalSkills!.map((x) => x)),
    "difficulty": difficulty,
    "numberOfDays": numberOfDays,
    "headerImage": headerImage?.toJson(),
    "locationIDs": locationIDs,
    "categoryIDs": categoryIDs,
    "createdAt": createdAt,
    "updatedAt": updatedAt,
  };
}


class Session {
  String? id;
  int? adaloId;
  String? title;
  String? subtitle;
  Images? image;
  List<String>? locationIDs;
  dynamic categoryIDs;
  dynamic subCategoryIDs;
  List<dynamic>? courseId;
  List<dynamic>? augmentedAudios;
  dynamic similarSessions;
  Description? description;
  bool? isPremium;
  bool? isTopRated;
  bool? isAugmented;
  String? difficulty;
  int? order;
  int? courseOrder;
  List<Content>? content;
  Attributes? attributes;
  AudioRequirements? audioRequirements;
  VoicedBy? voicedBy;
  double? rating;
  double? createdAt;
  double? updatedAt;

  Session({
    this.id,
    this.adaloId,
    this.title,
    this.subtitle,
    this.image,
    this.locationIDs,
    this.categoryIDs,
    this.subCategoryIDs,
    this.courseId,
    this.augmentedAudios,
    this.similarSessions,
    this.description,
    this.isPremium,
    this.isTopRated,
    this.isAugmented,
    this.difficulty,
    this.order,
    this.courseOrder,
    this.content,
    this.attributes,
    this.audioRequirements,
    this.voicedBy,
    this.rating,
    this.createdAt,
    this.updatedAt,
  });

  factory Session.fromJson(Map<String, dynamic> json) => Session(
    id: json["_id"],
    adaloId: json["adaloId"],
    title: json["title"],
    subtitle: json["subtitle"],
    image: json["image"] == null ? null : Images.fromJson(json["image"]),
    locationIDs: json["locationIDs"] == null ? [] : List<String>.from(json["locationIDs"]!.map((x) => x)),
    categoryIDs: json["categoryIDs"],
    subCategoryIDs: json["subCategoryIDs"],
    courseId: json["courseID"] == null ? [] : List<dynamic>.from(json["courseID"]!.map((x) => x)),
    augmentedAudios: json["augmentedAudios"] == null ? [] : List<dynamic>.from(json["augmentedAudios"]!.map((x) => x)),
    similarSessions: json["similarSessions"],
    description: json["description"] == null ? null : Description.fromJson(json["description"]),
    isPremium: json["isPremium"],
    isTopRated: json["isTopRated"],
    isAugmented: json["isAugmented"],
    difficulty: json["difficulty"],
    order: json["order"],
    courseOrder: json["courseOrder"],
    content: json["content"] == null ? [] : List<Content>.from(json["content"]!.map((x) => Content.fromJson(x))),
    attributes: json["attributes"] == null ? null : Attributes.fromJson(json["attributes"]),
    audioRequirements: json["audioRequirements"] == null ? null : AudioRequirements.fromJson(json["audioRequirements"]),
    voicedBy: json["voicedBy"] == null ? null : VoicedBy.fromJson(json["voicedBy"]),
    rating: json["rating"]?.toDouble(),
    createdAt: json["createdAt"]?.toDouble(),
    updatedAt: json["updatedAt"]?.toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "adaloId": adaloId,
    "title": title,
    "subtitle": subtitle,
    "image": image?.toJson(),
    "locationIDs": locationIDs == null ? [] : List<dynamic>.from(locationIDs!.map((x) => x)),
    "categoryIDs": categoryIDs,
    "subCategoryIDs": subCategoryIDs,
    "courseID": courseId == null ? [] : List<dynamic>.from(courseId!.map((x) => x)),
    "augmentedAudios": augmentedAudios == null ? [] : List<dynamic>.from(augmentedAudios!.map((x) => x)),
    "similarSessions": similarSessions,
    "description": description?.toJson(),
    "isPremium": isPremium,
    "isTopRated": isTopRated,
    "isAugmented": isAugmented,
    "difficulty": difficulty,
    "order": order,
    "courseOrder": courseOrder,
    "content": content == null ? [] : List<dynamic>.from(content!.map((x) => x.toJson())),
    "attributes": attributes?.toJson(),
    "audioRequirements": audioRequirements?.toJson(),
    "voicedBy": voicedBy?.toJson(),
    "rating": rating,
    "createdAt": createdAt,
    "updatedAt": updatedAt,
  };
}