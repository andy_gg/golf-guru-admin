
import 'package:data_uploader/models/audio.dart';

class Locations {
   String? id;
   int? adaloId;
   int? driveTime;
   String? name;
   Images? image;

  Locations({
    this.id,
    this.adaloId,
    this.driveTime,
    this.name,
    this.image,
  });

  factory Locations.fromJson(Map<String, dynamic> json) => Locations(
    id: json["_id"],
    adaloId: json["adaloId"],
    driveTime: json["driveTime"],
    name: json["name"],
    image: json["image"] == null ? null : Images.fromJson(json["image"]),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "adaloId": adaloId,
    "driveTime": driveTime,
    "name": name,
    "image": image?.toJson(),
  };
}


