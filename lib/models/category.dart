

import 'package:data_uploader/models/audio.dart';

class Category {
   String? id;
   int? adaloId;
   String? name;
   String? description;
   Images? image;
   int? order;
   List<SubCategory>? subCategories;

  Category({
    this.id,
    this.adaloId,
    this.name,
    this.description,
    this.image,
    this.order,
    this.subCategories,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["_id"],
    adaloId: json["adaloId"],
    name: json["name"],
    description: json["description"],
    image: json["image"] == null ? null : Images.fromJson(json["image"]),
    order: json["order"],
    subCategories: json["subCategories"] == null ? [] : List<SubCategory>.from(json["subCategories"]!.map((x) => SubCategory.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "adaloId": adaloId,
    "name": name,
    "description": description,
    "image": image?.toJson(),
    "order": order,
    "subCategories": subCategories == null ? [] : List<dynamic>.from(subCategories!.map((x) => x.toJson())),
  };
}



class SubCategory {
   int? adaloId;
   String? name;
   String? description;
   Images? image;
   int? order;
   List<int>? audioIds;

  SubCategory({
    this.adaloId,
    this.name,
    this.description,
    this.image,
    this.order,
    this.audioIds,
  });

  factory SubCategory.fromJson(Map<String, dynamic> json) => SubCategory(
    adaloId: json["adaloId"],
    name: json["name"],
    description: json["description"],
    image: json["image"] == null ? null : Images.fromJson(json["image"]),
    order: json["order"],
    audioIds: json["audioIds"] == null ? [] : List<int>.from(json["audioIds"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "adaloId": adaloId,
    "name": name,
    "description": description,
    "image": image?.toJson(),
    "order": order,
    "audioIds": audioIds == null ? [] : List<dynamic>.from(audioIds!.map((x) => x)),
  };
}


