

import 'package:data_uploader/models/audio.dart';

import 'course.dart';

class ProfileType {
   String? id;
   String? personalityType;
   SuggestionContent? suggestionContent;

  ProfileType({
    this.id,
    this.personalityType,
    this.suggestionContent,
  });

  factory ProfileType.fromJson(Map<String, dynamic> json) => ProfileType(
    id: json["_id"],
    personalityType: json["personalityType"],
    suggestionContent: json["suggestionContent"] == null ? null : SuggestionContent.fromJson(json["suggestionContent"]),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "personalityType": personalityType,
    "suggestionContent": suggestionContent?.toJson(),
  };
}

class SuggestionContent {
   List<Session>? sessions;
   List<Course>? courses;

  SuggestionContent({
    this.sessions,
    this.courses,
  });

  factory SuggestionContent.fromJson(Map<String, dynamic> json) => SuggestionContent(
    sessions: json["sessions"] == null ? [] : List<Session>.from(json["sessions"]!.map((x) => Session.fromJson(x))),
    courses: json["courses"] == null ? [] : List<Course>.from(json["courses"]!.map((x) => Course.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "sessions": sessions == null ? [] : List<dynamic>.from(sessions!.map((x) => x.toJson())),
    "courses": courses == null ? [] : List<dynamic>.from(courses!.map((x) => x.toJson())),
  };
}








class AudioRequirements {
   int? numberOfBalls;

  AudioRequirements({
    this.numberOfBalls,
  });

  factory AudioRequirements.fromJson(Map<String, dynamic> json) => AudioRequirements(
    numberOfBalls: json["numberOfBalls"],
  );

  Map<String, dynamic> toJson() => {
    "numberOfBalls": numberOfBalls,
  };
}

class Content {
   String? language;
   String? gender;
   String? url;
   String? previewUrl;
   int? lengthMins;

  Content({
    this.language,
    this.gender,
    this.url,
    this.previewUrl,
    this.lengthMins,
  });

  factory Content.fromJson(Map<String, dynamic> json) => Content(
    language: json["language"],
    gender: json["gender"],
    url: json["url"],
    previewUrl: json["previewUrl"],
    lengthMins: json["lengthMins"],
  );

  Map<String, dynamic> toJson() => {
    "language": language,
    "gender": gender,
    "url": url,
    "previewUrl": previewUrl,
    "lengthMins": lengthMins,
  };
}

class Description {
   String? long;
   String? short;

  Description({
    this.long,
    this.short,
  });

  factory Description.fromJson(Map<String, dynamic> json) => Description(
    long: json["long"],
    short: json["short"],
  );

  Map<String, dynamic> toJson() => {
    "long": long,
    "short": short,
  };
}

class VoicedBy {
   String? name;
   Images? image;

  VoicedBy({
    this.name,
    this.image,
  });

  factory VoicedBy.fromJson(Map<String, dynamic> json) => VoicedBy(
    name: json["name"],
    image: json["image"] == null ? null : Images.fromJson(json["image"]),
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "image": image?.toJson(),
  };
}
