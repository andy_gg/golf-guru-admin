import '../requests/audio_request.dart';

class Audio {
  String? sId;
  int? adaloId;
  String? title;
  String? subtitle;
  Images? image;
  List<String>? locationIDs;
  List<String>? categoryIDs;
  Null? subCategoryIDs;
  List<String>? courseID;
  List<String>? augmentedAudios;
  Description? description;
  bool? isPremium;
  bool? isTopRated;
  bool? isAugmented;
  String? difficulty;
  int? order;
  int? courseOrder;
  List<Content>? content;
  Attributes? attributes;
  AudioRequirements? audioRequirements;
  VoicedBy? voicedBy;
  num? rating;

  Audio(
      {this.sId,
        this.adaloId,
        this.title,
        this.subtitle,
        this.image,
        this.locationIDs,
        this.categoryIDs,
        this.subCategoryIDs,
        this.courseID,
        this.augmentedAudios,
        this.description,
        this.isPremium,
        this.isTopRated,
        this.isAugmented,
        this.difficulty,
        this.order,
        this.courseOrder,
        this.content,
        this.attributes,
        this.audioRequirements,
        this.voicedBy,
        this.rating});

  Audio.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    adaloId = json['adaloId'];
    title = json['title'];
    subtitle = json['subtitle'];
    image = json['image'] != null ? Images.fromJson(json['image']) : null;
    if(json['locationIDs'] !=null){
      locationIDs = json['locationIDs']?.cast<String>();
    }

    // if (json['categoryIDs'] != null) {
    //   categoryIDs = <Null>[];
    //   json['categoryIDs'].forEach((v) {
    //     categoryIDs!.add(Null.fromJson(v));
    //   });
    // }
    subCategoryIDs = json['subCategoryIDs'];
   // courseID = json['courseID'];
    //augmentedAudios = json['augmentedAudios'];
    description = json['description'] != null
        ? Description.fromJson(json['description'])
        : null;
    isPremium = json['isPremium'];
    isTopRated = json['isTopRated'];
    isAugmented = json['isAugmented'];
    difficulty = json['difficulty'];
    order = json['order'];
    courseOrder = json['courseOrder'];
    if (json['content'] != null) {
      content = <Content>[];
      json['content'].forEach((v) {
        content!.add(Content.fromJson(v));
      });
    }
    attributes = json['attributes'] != null
        ? Attributes.fromJson(json['attributes'])
        : null;
    audioRequirements = json['audioRequirements'] != null
        ? AudioRequirements.fromJson(json['audioRequirements'])
        : null;
    voicedBy = json['voicedBy'] != null
        ? VoicedBy.fromJson(json['voicedBy'])
        : null;
    rating = json['rating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = sId;
    data['adaloId'] = adaloId;
    data['title'] = title;
    data['subtitle'] = subtitle;
    if (image != null) {
      data['image'] = image!.toJson();
    }
    data['locationIDs'] = locationIDs;
    // if (categoryIDs != null) {
    //   data['categoryIDs'] = categoryIDs!.map((v) => v.toJson()).toList();
    // }
    data['subCategoryIDs'] = subCategoryIDs;
    data['courseID'] = courseID;
    data['augmentedAudios'] = augmentedAudios;
    if (description != null) {
      data['description'] = description!.toJson();
    }
    data['isPremium'] = isPremium;
    data['isTopRated'] = isTopRated;
    data['isAugmented'] = isAugmented;
    data['difficulty'] = difficulty;
    data['order'] = order;
    data['courseOrder'] = courseOrder;
    if (content != null) {
      data['content'] = content!.map((v) => v.toJson()).toList();
    }
    if (attributes != null) {
      data['attributes'] = attributes!.toJson();
    }
    if (audioRequirements != null) {
      data['audioRequirements'] = audioRequirements!.toJson();
    }
    if (voicedBy != null) {
      data['voicedBy'] = voicedBy!.toJson();
    }
    data['rating'] = rating;
    return data;
  }
}

class Images {
  String? url;
  int? size;
  int? width;
  int? height;
  String? filename;

  Images({this.url, this.size, this.width, this.height, this.filename});

  Images.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    size = json['size'];
    width = json['width'];
    height = json['height'];
    filename = json['filename'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['url'] = url;
    data['size'] = size;
    data['width'] = width;
    data['height'] = height;
    data['filename'] = filename;
    return data;
  }
}

class Attributes {
  int? focus;
  int? confidence;
  int? calm;
  bool? isDrill;
  int? balls;

  Attributes(
      {this.focus, this.confidence, this.calm, this.isDrill, this.balls});

  Attributes.fromJson(Map<String, dynamic> json) {
    focus = json['focus'];
    confidence = json['confidence'];
    calm = json['calm'];
    isDrill = json['isDrill'];
    balls = json['balls'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['focus'] = focus;
    data['confidence'] = confidence;
    data['calm'] = calm;
    data['isDrill'] = isDrill;
    data['balls'] = balls;
    return data;
  }
}
