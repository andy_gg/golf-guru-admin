import 'dart:convert';
import 'dart:core';
import 'package:data_uploader/models/category.dart';
import 'package:data_uploader/models/locations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';

import '../app_common_button.dart';
import '../app_constants.dart';
import '../models/audio.dart';
import '../models/course.dart';
import '../requests/audio_request.dart';
import '../utilities/utilities.dart';
import 'audio_uploader_view_model.dart';

class AudioUploader extends StatefulWidget {
  bool isEditing;
  AudioUploader({Key? key, this.isEditing = false}) : super(key: key);
  @override
  State<AudioUploader> createState() => _AudioUploaderState();
}

class _AudioUploaderState extends State<AudioUploader> {
  // AudioUploaderViewModel viewModel = AudioUploaderViewModel();
  final _formKey = GlobalKey<FormBuilderState>();
  final _formKeyDropdown = GlobalKey<FormFieldState>();
  List<String> genderOptions = ['male', 'male', 'male'];
  List<String> courseIds = [];
  Course? course;
  List<Category> categoryIds = [];
  Category? category;
  List<Locations> locationIds = [];
  List<Audio> augmentedAudioIds = [];
  List<String> mentalSkills = [];
  List<SimilarSessions> similarSessionModels = [];
  String? title;
  Audio? selectedAudio;
  String? drillInput;
  bool isSelected = false;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.twilightBackground,
        // appBar: AppBar(
        //   backgroundColor: AppColors.mainLightGreen,
        //   title: Text('Audio Session ${widget.isEditing ? "Update" : "Upload"}'),
        // ),
        body: SafeArea(
          child: Center(
      child: Container(
          width: 600,


          child: SingleChildScrollView(
            padding: const EdgeInsets.all(30.0),
            child: ViewModelBuilder<UploaderViewModel>.nonReactive(
                viewModelBuilder: () => UploaderViewModel(),
                onViewModelReady: (UploaderViewModel model) => model.setup(),
                builder: (BuildContext context, UploaderViewModel model,
                    Widget? child) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      FormBuilder(
                        key: _formKey,
                        autovalidateMode: AutovalidateMode.always,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ...[
                              const SizedBox(
                                height: 0,
                              ),
                              BasicButton(
                                  padding: const EdgeInsets.all(3),
                                  child: SvgPicture.asset(
                                    AppAssets.backIcon,
                                    height: 46,
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  }),
                              Text(
                                'Audio Session ${widget.isEditing ? "Update" : "Upload"}',
                                style: const TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white),
                              ),
                              const SizedBox(
                                height: 30,
                              ),
                              if (widget.isEditing)
                                Consumer<UploaderViewModel>(
                                    builder: (context, value, child) {
                                  return SizedBox(
                                    height: 55,
                                    child: DropdownButtonFormField<Audio>(
                                      dropdownColor: Color(0xFF1C1B1F),
                                      decoration: InputDecoration(
                                        contentPadding: const EdgeInsets.all(8),
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8.0),
                                          borderSide: const BorderSide(
                                            color: Color(0xffD5D4DC),
                                            width: 1.0,
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(5.0),
                                          borderSide: const BorderSide(
                                            color: Color(0xffD5D4DC),
                                            width: 1.0,
                                          ),
                                        ),
                                      ),
                                      hint: const Text(
                                        'Find Session - (Get all sessions)',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      value: selectedAudio,
                                      icon: const Icon(
                                        Icons.keyboard_arrow_down_sharp,
                                        size: 32,
                                        color: Colors.white,
                                      ),
                                      elevation: 16,

                                      isExpanded: true,
                                      onChanged: (Audio? value) {
                                        setState(() {

                                          selectedAudio = value!;

                                          _formKey.currentState?.fields['title']?.didChange(selectedAudio?.title ?? "");
                                          _formKey.currentState?.fields['subtitle']?.didChange(selectedAudio?.subtitle ?? "");
                                          _formKey.currentState?.fields['Difficulty']?.didChange(selectedAudio?.difficulty ?? "");
                                          _formKey.currentState?.fields['courseOrder']?.didChange(selectedAudio?.courseOrder.toString() ?? "");
                                         // _formKey.currentState?.fields['rating']?.didChange(selectedAudio?.rating.toString() ?? "");
                                          _formKey.currentState?.fields['audioImage']?.didChange(selectedAudio?.image?.url ?? "");
                                          _formKey.currentState?.fields['description']?.didChange(selectedAudio?.description?.short.toString() ?? "");
                                          if ((selectedAudio?.content != null) && selectedAudio?.content?.isNotEmpty == true) {
                                            _formKey.currentState?.fields['url']?.didChange(selectedAudio?.content?[0].url ?? "");
                                            _formKey.currentState?.fields['lengthMins']?.didChange(selectedAudio?.content
                                                        ?.first.lengthMins.toString() ?? "");
                                          }
                                          _formKey.currentState?.fields['numberOfBalls']?.didChange(selectedAudio?.audioRequirements
                                                      ?.numberOfBalls.toString() ?? "");
                                          _formKey.currentState?.fields['voicedByName']?.didChange(
                                                  selectedAudio?.voicedBy?.name ??
                                                      "");
                                          _formKey.currentState
                                              ?.fields['voicedByImage']
                                              ?.didChange(selectedAudio
                                                      ?.voicedBy?.image?.url ??
                                                  "");
                                          _formKey
                                              .currentState?.fields['isPremium']
                                              ?.didChange(selectedAudio?.isPremium ?? false);
                                          _formKey
                                              .currentState?.fields['isTopRated']
                                              ?.didChange(
                                                  selectedAudio?.isTopRated ??
                                                      false);
                                          _formKey
                                              .currentState?.fields['IsAugmented']
                                              ?.didChange(
                                                  selectedAudio?.isAugmented ??
                                                      false);
                                          mentalSkills.clear();
                                          if (selectedAudio?.attributes?.calm ==
                                              1) {
                                            mentalSkills.add('Calm');
                                          }
                                          if (selectedAudio
                                                  ?.attributes?.confidence ==
                                              1) {
                                            mentalSkills.add('Confidence');
                                          }
                                          if (selectedAudio?.attributes?.focus ==
                                              1) {
                                            mentalSkills.add('Focus');
                                          }
                                          if ((selectedAudio?.courseID != null) &&
                                              selectedAudio?.courseID?.isNotEmpty == true) {
                                            course = model.courses.firstWhere(
                                                (element) => element.id ==selectedAudio?.courseID?.first);
                                            courseIds = [course!.id!];
                                          }
                                          if ((selectedAudio?.locationIDs != null) && selectedAudio?.locationIDs?.isNotEmpty == true) {
                                            locationIds = model.locations.where((element) => selectedAudio?.locationIDs?.contains(element.id) == true).toList();
                                          }

                                          if ((selectedAudio?.categoryIDs != null) && selectedAudio?.categoryIDs?.isNotEmpty == true) {
                                            categoryIds = model.categories.where((element) => selectedAudio?.categoryIDs?.contains(element.id) == true).toList();
                                          }
                                          if ((selectedAudio?.augmentedAudios != null) && selectedAudio?.augmentedAudios?.isNotEmpty == true) {
                                            augmentedAudioIds = model.audios.where((element) => selectedAudio?.augmentedAudios?.contains(element.sId) == true).toList();
                                          }

                                        });
                                      },

                                      items: model.audios
                                          .map<DropdownMenuItem<Audio>>(
                                              (Audio value) {
                                        return DropdownMenuItem<Audio>(
                                          value: value,
                                          child: Text(
                                            value.title.toString(),
                                            style: TextStyle(color: Colors.white),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  );
                                }),
                              if (widget.isEditing)
                                AppCommonButton(
                                    width: 362,
                                    height: 52,
                                    radius: 8,
                                    title: 'Get Session',
                                    onPressed: () {}

                                    ),
                              const SizedBox(height: 1,),
                              CustomTextField(name: 'title', label: 'Title',
                                initialValue: selectedAudio?.title,
                              ),
                              const CustomTextField(name: 'subtitle', label: 'Subtitle'),
                              const CustomTextField(name: 'audioImage', label: 'Image'),
                        //      const CustomTextField(name: 'url', label: 'Audio'),
                              const CustomTextField(name: 'description', label: 'Short Description'),
                              const CustomTextField(name: 'lengthMins', label: 'Length',
                                  keyboardType: TextInputType.numberWithOptions(decimal: true)),
                              SizedBox(
                                // height: 52,
                                width: 320,
                                child: MultiSelectDialogField(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(color: Colors.white),
                                  ),
                                  backgroundColor: const Color(0xff0B1F23),
                                  key: _formKeyDropdown,
                                  initialValue: List<String>.from(mentalSkills),
                                  buttonText: const Text('Mental Skills',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500)),
                                  buttonIcon: const Icon(
                                    Icons.arrow_drop_down,
                                    color: Color(0xff0B1F23),
                                  ),
                                  dialogHeight: genderOptions.length * 50,
                                  title: const Text(
                                    'Mental Skills',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  items: model.skills
                                      .map((e) => MultiSelectItem(e, e))
                                      .toList(),
                                  onSelectionChanged: (value) {
                                    //Navigator.of(context).pop();
                                  },
                                  selectedItemsTextStyle:
                                      const TextStyle(color: Colors.white),
                                  itemsTextStyle:
                                      const TextStyle(color: Colors.white),
                                  // listType: MultiSelectListType.CHIP,
                                  onConfirm: (values) {
                                    mentalSkills = values as List<String>;
                                  },
                                ),
                              ),
                              FormBuilderRadioGroup(
                                activeColor: Colors.white,
                                decoration: const InputDecoration(
                                  labelText: 'Difficulty',
                                  labelStyle: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500),
                                  border: InputBorder.none,
                                ),
                                name: 'Difficulty',

                                options: model.difficulty
                                    .map((lang) =>
                                        FormBuilderFieldOption(value: lang,))
                                    .toList(growable: false),
                              ),
                             /* Consumer<UploaderViewModel>(
                                  builder: (context, value, child) {
                                    return Container(
                                      height: 52,
                                      width: 320,
                                      padding: EdgeInsets.zero,
                                      child: DropdownButtonFormField<Category>(
                                        icon: const SizedBox.shrink(),
                                        dropdownColor: Color(0xFF1C1B1F),
                                        decoration: InputDecoration(
                                          contentPadding: const EdgeInsets.all(8),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8.0),
                                            borderSide: const BorderSide(
                                              color: Color(0xffD5D4DC),
                                              width: 1.0,
                                            ),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5.0),
                                            borderSide: const BorderSide(
                                              color: Color(0xffD5D4DC),
                                              width: 1.0,
                                            ),
                                          ),
                                        ),
                                        isExpanded: true,

                                        value: category,
                                        onChanged: (Category? newValue) {
                                          setState(() {
                                            category = newValue;
                                          });
                                          categoryIds = [newValue!];
                                        },
                                        focusColor: Colors.lightBlue,
                                        items: model.categories.map((Category e) {
                                          return DropdownMenuItem<Category>(
                                            value: e,
                                            child: Text(e.name ?? "",
                                                style: const TextStyle(
                                                    fontSize: 16.0,
                                                    color: Colors.white)),
                                          );
                                        }).toList(),
                                        hint: const Text(
                                          'Category IDs',
                                          style: TextStyle(
                                              fontSize: 16.0, color: Colors.white),
                                        ),

                                      ),
                                    );
                                  }),*/
                              Consumer<UploaderViewModel>(
                                  builder: (context, value, child) {
                                    return SizedBox(
                                      width: 320,

                                      child: MultiSelectDialogField(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(8),
                                          border: Border.all(color: Colors.white),
                                        ),
                                        backgroundColor: Color(0xFF1C1B1F),
                                        selectedItemsTextStyle:
                                        const TextStyle(color: Colors.white),
                                        itemsTextStyle:
                                        const TextStyle(color: Colors.white),
                                        initialValue:
                                        List<Category>.from(categoryIds),
                                        buttonText: const Text('Category IDs',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500)),
                                        buttonIcon: const Icon(
                                          Icons.arrow_drop_down,
                                          color: Colors.white,
                                        ),
                                        dialogHeight: (model.categories.length * 50) + 20,
                                        title: const Text(
                                          'Category IDs',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        items:model.categories.map((e) => MultiSelectItem(e, e.name ?? '')).toList(),

                                        onConfirm: ( List<Category> values) {
                                          categoryIds = values.toList();

                                        },

                                      ),
                                    );
                                  }),
                              Consumer<UploaderViewModel>(
                                  builder: (context, value, child) {
                                    return SizedBox(
                                      width: 320,

                                      child: MultiSelectDialogField(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(8),
                                          border: Border.all(color: Colors.white),
                                        ),
                                        backgroundColor: Color(0xFF1C1B1F),
                                        selectedItemsTextStyle:
                                        const TextStyle(color: Colors.white),
                                        itemsTextStyle:
                                        const TextStyle(color: Colors.white),
                                        initialValue:
                                        List<Locations>.from(locationIds),
                                        buttonText: const Text('Location IDs',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500)),
                                        buttonIcon: const Icon(
                                          Icons.arrow_drop_down,
                                          color: Colors.white,
                                        ),
                                        dialogHeight: (model.locations.length * 50) + 20,
                                        title: const Text(
                                          'Location IDs',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        items:model.locations.map((e) => MultiSelectItem(e, e.name ?? '')).toList(),

                                        onConfirm: ( List<Locations> values) {
                                          locationIds = values.toList();
                                          // locationIds = values.map((e) => e.sId ?? '').toList();
                                        },

                                      ),
                                    );
                                  }),
                              const CustomTextField(
                                  name: 'courseOrder', label: 'Course Order'),

                              Consumer<UploaderViewModel>(
                                  builder: (context, value, child) {
                                return SizedBox(
                                  width: 320,

                                  child: MultiSelectDialogField(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      border: Border.all(color: Colors.white),
                                    ),
                                    backgroundColor: Color(0xFF1C1B1F),
                                    selectedItemsTextStyle:
                                        const TextStyle(color: Colors.white),
                                    itemsTextStyle:
                                        const TextStyle(color: Colors.white),
                                    initialValue:
                                        List<Audio>.from(augmentedAudioIds),
                                    buttonText: const Text('Augmented Audio IDs',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500)),
                                    buttonIcon: const Icon(
                                      Icons.arrow_drop_down,
                                      color: Colors.white,
                                    ),
                                    dialogHeight: (model.audios.length * 50) + 20,
                                    title: const Text(
                                      'Augmented Audio IDs',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    items: model.audios
                                        .map((e) =>
                                            MultiSelectItem(e, e.title ?? ''))
                                        .toList(),
                                    onConfirm: (List<Audio> values) {
                                      augmentedAudioIds = values.toList();
                                    },
                                  ),
                                );
                              }),


                              FormBuilderCheckbox(
                                name: 'isPremium',
                                side: const BorderSide(color: Colors.white),
                                initialValue: model.isPremium,
                                onChanged: model.setPremium,
                                title: const Text(
                                  'IsPremium',
                                  style: TextStyle(color: Colors.white),
                                ),

                              ),
                              FormBuilderCheckbox(
                                name: 'isTopRated',
                                initialValue: model.isTopRated,
                                onChanged: model.setTopRated,
                                side: const BorderSide(color: Colors.white),
                                title: const Text(
                                  'IsTopRated',
                                  style: TextStyle(color: Colors.white),
                                ),

                              ),
                              FormBuilderCheckbox(
                                name: 'IsAugmented',
                                side: const BorderSide(color: Colors.white),
                                initialValue: model.isAugmented,
                                onChanged: model.setAugmented,
                                title: const Text(
                                  'IsAugmented',
                                  style: TextStyle(color: Colors.white),
                                ),

                              ),
                              FormBuilderCheckbox(
                                name: 'IsADrill',
                                side: const BorderSide(color: Colors.white),
                                initialValue: model.isDrill,
                                onChanged: model.setDrill,
                                title: const Text(
                                  'Is a Drill',
                                  style: TextStyle(color: Colors.white),
                                ),

                              ),
                              const CustomTextField(name: 'numberOfBalls', label: 'Number of Balls', keyboardType: TextInputType.number),
                              const CustomTextField(name: 'voicedByName', label: 'Voiced By'),
                              const CustomTextField(name: 'voicedByImage', label: 'Voiced By Image'),

                              Consumer<UploaderViewModel>(
                                  builder: (context, value, child) {
                                return SizedBox(
                                  height: 52,
                                  width: 320,
                                  child: DropdownButtonFormField<String>(
                                    dropdownColor: const Color(0xFF1C1B1F),

                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.all(8),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8.0),
                                        borderSide: const BorderSide(
                                          color: Color(0xffD5D4DC),
                                          width: 1.0,
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8.0),
                                        borderSide: const BorderSide(
                                          color: Color(0xffD5D4DC),
                                          width: 1.0,
                                        ),
                                      ),
                                    ),
                                    hint: const Text(
                                      'Drill Data input type',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    value: drillInput,
                                    icon: const Icon(
                                      Icons.keyboard_arrow_down_sharp,
                                      size: 35,
                                      color: Colors.white,
                                    ),
                                    elevation: 16,

                                    isExpanded: true,
                                    onChanged: (String? value) {
                                      drillInput = value!;
                                    },

                                    items: model.drillInput.map<DropdownMenuItem<String>>((String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(
                                          value.toString(),
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      );
                                    }).toList(),
                                    borderRadius: BorderRadius.circular(8),

                                  ),
                                );
                              }),
                            ].expand(
                              (widget) => [
                                widget,
                                const SizedBox(
                                  height: 15,
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 60,
                      ),
                      AppCommonButton(
                          width: 362,
                          height: 52,
                          radius: 8,
                          title: widget.isEditing
                              ? 'Preview Changes'
                              : 'Preview Upload',
                          onPressed: () {
                            setState(() {
                              isSelected = !isSelected;
                            });

                            if (!widget.isEditing) {

                              selectedAudio = Audio();
                              selectedAudio?.title = _formKey.currentState?.fields['title']?.value ?? "";
                              selectedAudio?.isPremium = _formKey.currentState?.fields['isPremium']?.value ?? false;
                              Images image = Images();
                              image.url = _formKey.currentState?.fields['audioImage']?.value ?? "";
                              selectedAudio?.image = image;
                              VoicedBy voiceBy =VoicedBy();
                              voiceBy.image?.url=_formKey.currentState
                                  ?.fields['voicedByImage']?.value ?? "";
                              selectedAudio?.voicedBy=voiceBy;
                              Content content = Content();
                              content.lengthMins = int.tryParse(_formKey.currentState?.fields['lengthMins']?.value ?? "") ?? 0;
                              selectedAudio?.content ??= [];
                              selectedAudio?.content?.add(content);
                            }

                          }),
                      isSelected == true
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(
                                  height: 50,
                                ),
                                const Text(
                                  'Audio Preview',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white),
                                ),
                                const SizedBox(
                                  height: 30,
                                ),
                               Visibility(
                                   visible: selectedAudio!=null,
                                   child:  customAudioCard(
                                     imgUrl: selectedAudio?.voicedBy?.image?.url??"https://ucarecdn.com/60975c09-81c1-4c89-a3ad-8fe5393bf7a1/MotivationForSwingChanges.jpg",
                                   isPremium: selectedAudio?.isPremium ??
                                       false,
                                   context: context,
                                   img: selectedAudio?.image?.url
                                       ?.toString()??"https://ucarecdn.com/60975c09-81c1-4c89-a3ad-8fe5393bf7a1/MotivationForSwingChanges.jpg",
                                   title: selectedAudio?.title??"",
                                   subTitle: selectedAudio
                                       ?.content?.first.lengthMins
                                       .toString() ??
                                       "",
                                   onTap: () {})),
                                const SizedBox(
                                  height: 80,
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: AppCommonButton(
                                        width: 362,
                                        height: 55,
                                        radius: 8,
                                        // title: widget.isEditing ? 'Update' : 'Publish',
                                        title: 'Publish Changes',
                                        onPressed: () async {
                                          _formKey.currentState?.save();
                                          if (!(_formKey.currentState
                                                  ?.validate() ??
                                              false)) {
                                            return;
                                          }
                                          debugPrint(_formKey.currentState?.value
                                              .toString());
                                          Utilities.showSnack(
                                              context, 'Creating session...');
                                          try {
                                            AudioRequest request = AudioRequest();
                                            request.title = _formKey
                                                .currentState?.value['title'];
                                            request.subtitle = _formKey
                                                .currentState?.value['subtitle'];
                                            request.difficulty = _formKey.currentState?.value['Difficulty'];
                                            request.courseID = courseIds;
                                            request.categoryIDs = categoryIds.map((e) => e.id ?? '').toList();
                                            request.augmentedAudios = augmentedAudioIds.map((e) => e.sId ?? '').toList();
                                            request.similarSessions = similarSessionModels;
                                            request.courseOrder = int.parse(_formKey.currentState?.value['courseOrder']);
                                       //     request.rating = double.parse(_formKey.currentState?.value['rating']);
                                            ImageModel audioImage = ImageModel();
                                            audioImage.url = _formKey.currentState
                                                ?.value['audioImage'];
                                            request.image = audioImage;

                                            Description description =
                                                Description();
                                            description.short = _formKey
                                                .currentState
                                                ?.value['description'];
                                            request.description = description;

                                            Content content = Content();
                                            content.url = _formKey
                                                    .currentState?.value['url'] ??
                                                "";
                                            content.lengthMins = int.parse(
                                                _formKey.currentState
                                                    ?.value['lengthMins']??"");

                                            request.content = [content];
                                            AudioRequirements audioRequirements= AudioRequirements();
                                            audioRequirements
                                                .numberOfBalls = int.parse(_formKey
                                                .currentState
                                                ?.value['numberOfBalls'] ??
                                                '');
                                            request.audioRequirements=audioRequirements;
                                            Attributes attributes = Attributes();
                                            if (mentalSkills.contains('Calm')) {
                                              attributes.calm = 1;
                                            }
                                            if (mentalSkills
                                                .contains('Confidence')) {
                                              attributes.confidence = 1;
                                            }
                                            if (mentalSkills.contains('Focus')) {
                                              attributes.focus = 1;
                                            }
                                            attributes.isDrill=_formKey.currentState?.value['IsADrill'];
                                            request.attributes = attributes;

                                            VoicedBy voicedBy = VoicedBy();
                                            voicedBy.name = _formKey.currentState?.value['voicedByName'];
                                            ImageModel imageModel = ImageModel();
                                            imageModel.url = _formKey.currentState?.value['voicedByImage'];
                                            voicedBy.image = imageModel;
                                            request.voicedBy = voicedBy;
                                            request.locationID = locationIds.map((e) => e.id ?? '').toList();
                                            request.isPremium = _formKey.currentState?.value['isPremium'];
                                            request.isTopRated = _formKey.currentState?.value['isTopRated'];
                                            request.isAugmented = _formKey.currentState?.value['IsAugmented'];

                                            debugPrint(json.encode(request.toJson()).toString());
                                            if (widget.isEditing && selectedAudio != null) {
                                              await model.updateAudioSession(
                                                  request,
                                                  selectedAudio?.sId ?? '');
                                            } else {
                                              await model.createAudioSession(request);
                                            }
                                            Utilities.showSnack(context,
                                                'Audio session created successfully');
                                            Navigator.of(context).pop();
                                          } catch (error, s) {
                                            Utilities.showSnack(
                                                context, error.toString());

                                            print("error:::${s.toString()}");
                                          }
                                        },
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            )
                          : const SizedBox(
                              height: 10,
                            ),
                    ],
                  );
                }),
          ),
      ),
    ),
        ));
  }
}

class CustomTextField extends StatefulWidget {
  final String name;
  final String label;

  final TextInputType keyboardType;
  final String? initialValue;
  final ValueChanged<String?>? onChanged;
  const CustomTextField({
    Key? key,
    required this.name,
    required this.label,
    this.keyboardType = TextInputType.name,
    this.initialValue, this.onChanged,
  }) : super(key: key);

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  FocusNode myFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 320,
      height: 52,
      child: FormBuilderTextField(
        cursorColor: Colors.white,
        style: const TextStyle(color: Colors.white),
        initialValue: widget.initialValue ?? "",
        name: widget.name,
        onChanged: widget.onChanged,
        decoration: InputDecoration(
          hintText: widget.label,
          contentPadding: const EdgeInsets.all(8),
          hintStyle: const TextStyle(
              color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500),
          // floatingLabelStyle: const TextStyle(color: AppColors.mainLightGreen),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.0),
            borderSide: const BorderSide(
              color: Color(0xffD5D4DC),
              width: 1.0,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.0),
            borderSide: const BorderSide(
              color: Color(0xffD5D4DC),
              width: 1.0,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.0),
            borderSide: const BorderSide(
              color: Color(0xffD5D4DC),
              width: 1.0,
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.0),
            borderSide: const BorderSide(
              color: Color(0xffD5D4DC),
              width: 1.0,
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.0),
            borderSide: const BorderSide(
              color: Color(0xffD5D4DC),
              width: 1.0,
            ),
          ),
        ),
        //onChanged: _onChanged,
        validator: FormBuilderValidators.compose([
          //FormBuilderValidators.required(context),
        ]),
        keyboardType: widget.keyboardType,
      ),
    );
  }
}

// void _showMultiSelect(BuildContext context) async {
//   final items = <MultiSelectDialogItem<int>>[
//     const MultiSelectDialogItem(1, 'Dog'),
//     const MultiSelectDialogItem(2, 'Cat'),
//     const MultiSelectDialogItem(3, 'Mouse'),
//     const MultiSelectDialogItem(1, 'Dog'),
//     const MultiSelectDialogItem(2, 'Cat'),
//     const MultiSelectDialogItem(3, 'Mouse'),
//     const MultiSelectDialogItem(1, 'Dog'),
//     const MultiSelectDialogItem(2, 'Cat'),
//     const MultiSelectDialogItem(3, 'Mouse'),
//     const MultiSelectDialogItem(1, 'Dog'),
//     const MultiSelectDialogItem(2, 'Cat'),
//     const MultiSelectDialogItem(3, 'Mouse'),
//     const MultiSelectDialogItem(1, 'Dog'),
//     const MultiSelectDialogItem(2, 'Cat'),
//     const MultiSelectDialogItem(3, 'Mouse'),
//   ];
//
//   final selectedValues = await showDialog<Set<int>>(
//     context: context,
//     builder: (BuildContext context) {
//       return MultiSelectDialog(
//         items: items,
//         initialSelectedValues: [1, 3].toSet(),
//       );
//     },
//   );
//
//   print(selectedValues);
// }

// class MultiSelectDialogItem<V> {
//   const MultiSelectDialogItem(this.value, this.label);
//
//   final V value;
//   final String label;
// }
//
// class MultiSelectDialog<V> extends StatefulWidget {
//   const MultiSelectDialog(
//       {Key? key, required this.items, required this.initialSelectedValues})
//       : super(key: key);
//
//   final List<MultiSelectDialogItem<V>> items;
//   final Set<V> initialSelectedValues;
//
//   @override
//   State<StatefulWidget> createState() => _MultiSelectDialogState<V>();
// }
//
// class _MultiSelectDialogState<V> extends State<MultiSelectDialog<V>> {
//   final _selectedValues = Set<V>();
//
//   void initState() {
//     super.initState();
//     if (widget.initialSelectedValues != null) {
//       _selectedValues.addAll(widget.initialSelectedValues);
//     }
//   }
//
//   void _onItemCheckedChange(V itemValue, bool checked) {
//     setState(() {
//       if (checked) {
//         _selectedValues.add(itemValue);
//       } else {
//         _selectedValues.remove(itemValue);
//       }
//     });
//   }
//
//   void _onCancelTap() {
//     Navigator.pop(context);
//   }
//
//   void _onSubmitTap() {
//     Navigator.pop(context, _selectedValues);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return AlertDialog(
//       title: const Text('Select animals'),
//       contentPadding: const EdgeInsets.only(top: 12.0),
//       content: SingleChildScrollView(
//         child: ListTileTheme(
//           contentPadding: const EdgeInsets.fromLTRB(14.0, 0.0, 24.0, 0.0),
//           child: ListBody(
//             children: widget.items.map(_buildItem).toList(),
//           ),
//         ),
//       ),
//       actions: <Widget>[
//         FlatButton(
//           child: const Text('CANCEL'),
//           onPressed: _onCancelTap,
//         ),
//         FlatButton(
//           child: const Text('OK'),
//           onPressed: _onSubmitTap,
//         )
//       ],
//     );
//   }
//
//   Widget _buildItem(MultiSelectDialogItem<V> item) {
//     final checked = _selectedValues.contains(item.value);
//     return CheckboxListTile(
//       value: checked,
//       title: Text(item.label),
//       controlAffinity: ListTileControlAffinity.leading,
//       onChanged: (checked) =>
//           _onItemCheckedChange(item.value, checked ?? false),
//     );
//   }
// }
