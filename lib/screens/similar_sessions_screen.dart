import 'package:flutter/material.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';

import '../app_constants.dart';
import '../models/audio.dart';
import 'audio_uploader_view_model.dart';

class SimilarSessionsScreen extends StatefulWidget {
  final UploaderViewModel uploaderViewModel;
  const SimilarSessionsScreen({Key? key, required this.uploaderViewModel}) : super(key: key);

  @override
  State<SimilarSessionsScreen> createState() => _SimilarSessionsScreenState();
}

class _SimilarSessionsScreenState extends State<SimilarSessionsScreen> {
  List<Audio> similarSessionsAudios = [];


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    similarSessionsAudios = widget.uploaderViewModel.similarSessions;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.mainLightGreen,
          title: const Text('Select Similar Sessions'),
        ),
        body: WillPopScope(
          onWillPop: () async {
            Navigator.pop(context, similarSessionsAudios);
            return false;
          },
          child: ViewModelBuilder<UploaderViewModel>.nonReactive(
              viewModelBuilder: () => widget.uploaderViewModel,
              disposeViewModel: false,
              onModelReady: (UploaderViewModel model) => model.setup(),
              builder:
                  (BuildContext context, UploaderViewModel model, Widget? child) {
                return Column(
                  children: [
                    Consumer<UploaderViewModel>(
                        builder: (context, value, child) {
                      return MultiSelectDialogField<Audio>(
                        initialValue: model.similarSessions,
                        chipDisplay: MultiSelectChipDisplay<Audio>.none(),
                        buttonText: const Text('Similar Sessions IDs'),
                        buttonIcon: const Icon(Icons.arrow_drop_down),
                        dialogHeight: (model.audios.length * 50) + 20,
                        title: const Text('Similar Sessions IDs'),
                        items: model.audios
                            .map((e) => MultiSelectItem(e, e.title ?? ''))
                            .toList(),
                        onConfirm: (List<Audio> values) {
                          setState(() {
                            values.sort((a, b) => similarSessionsAudios.indexOf(a).compareTo(similarSessionsAudios.indexOf(b)));
                            similarSessionsAudios = values;
                          });
                        },
                      );
                    }),
                    Expanded(
                      child: ReorderableListView(
                        onReorder: reorderData,
                        children: <Widget>[
                          for (final Audio items in similarSessionsAudios)
                            Card(
                              key: ValueKey(items),
                              elevation: 2,
                              child: ListTile(
                                title: Text(items.title ?? ''),
                                leading: const Icon(
                                  Icons.reorder,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                        ],
                      ),
                    ),
                  ],
                );
              }),
        ));
  }

  void reorderData(int oldindex, int newindex) {
    setState(() {
      if(newindex>oldindex){
        newindex-=1;
      }
      final items = similarSessionsAudios.removeAt(oldindex);
      similarSessionsAudios.insert(newindex, items);
    });
  }
}

extension IndexedIterable<E> on Iterable<E> {
  Iterable<T> mapIndexed<T>(T Function(E e, int i) f) {
    var i = 0;
    return map((e) => f(e, i++));
  }
}