import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:data_uploader/models/locations.dart';
import 'package:data_uploader/models/profile_type.dart';
import 'package:data_uploader/models/track_suggestions.dart';
import 'package:data_uploader/utilities/enum_type.dart';
import 'package:data_uploader/utilities/utilities.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../basemodel.dart';
import '../models/audio.dart';
import '../models/category.dart';
import '../models/course.dart';
import '../requests/audio_request.dart';
import '../requests/course_request.dart';

class UploaderViewModel extends BaseModel {
  var accessToken = 'LtXAvlb98r7lGDVNtO7x0a5aECBEpS6T6FTCy6me';
  var baseURL = 'https://admin.golf-guru.co.uk';
  List<String> skills = ['Calm', 'Confidence', 'Focus'];
  List<String> difficulty = ['Beginner', 'Intermediate', 'Expert'];
  List<String> drillInput = ['Score + Number of attempts','Number of Attempts','Over Under','Win Loose','Combined Scores' ];
  List<Audio> similarSessions = [];
  List<Locations> locations = [];
  List<Course> courses = [];
  List<Category> categories = [];
  late bool isPremium = false;
  late bool isTopRated = false;
  late bool isAugmented = false;
  late bool isDrill = false;
  List<Audio> audios = [];
  List<ProfileType> profileTypes = [];
  List<TrackSuggestions> tracksSuggestions = [];
  ProfileType? profileType;
  TrackSuggestions? tracksSuggestion;
 OverlayPortalController tooltipPTController = OverlayPortalController();
 OverlayPortalController tooltipSSController = OverlayPortalController();
 double? buttonWidth;
  setPremium(bool? value) {
    isPremium = value ?? false;
  }

  setTopRated(bool? value) {
    isTopRated = value ?? false;
  }

  setAugmented(bool? value) {
    isAugmented = value ?? false;
  }
  setDrill(bool? value) {
    isAugmented = value ?? false;
  }

  setup() {
    getData();

  }

  getData() async {
      //await getHttp();
     await getLocations();
    await getCourses();
    await getAudios();
    await getProfileType();
    await getCategories();
    await getTrackSuggestion();
  }

  getHttp() async {
    try {
      var response =
          await Dio().get('https://api-dev.vyng.me/api/v3/user/profile?clientData=1');
      print(response);
    } catch (e) {
      print(e);
    }
  }

  getLocations() {
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/admin/locations', method: 'GET', headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    http.get(Uri.parse('$baseURL/admin/locations'),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "x-api-key": accessToken
        }).then((http.Response response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400) {
        debugPrint(json.decode(res).toString());
      } else {
        debugPrint(json.decode(res).toString());
        final List parsed = await json.decode(res);
        locations = parsed.map((m) {
          return Locations.fromJson(m);
        }).toList();
        updateState();
      }
    });
  }

  getCourses() {
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/admin/courses', method: 'GET', headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    http.get(Uri.parse('$baseURL/admin/courses'), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    }).then((http.Response response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400) {
        log('Error in api');
      } else {
        log(json.decode(res).toString());
        final List parsed = await json.decode(res);
        courses = parsed.map((m) {
          return Course.fromJson(m);
        }).toList();
        courses = courses.where((element) => element.name?.isNotEmpty ?? false).toList();
        courses = courses.map((e) {
          e.name = e.name?.replaceAll("\n", " ");
          return e;
        }).toList();
        updateState();
      }
    });
  }

  getAudios() {
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/admin/audios', method: 'GET', headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    http.get(Uri.parse('$baseURL/admin/audios'), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    }).then((http.Response response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400) {
        log('Error in api');
      } else {
        log(json.decode(res).toString());
        final List parsed = await json.decode(res);
        audios = parsed.map((m) {
          return Audio.fromJson(m);
        }).toList();
        audios = audios.where((element) => element.title?.isNotEmpty ?? false).toList();
        audios = audios.map((e) {
          e.title = e.title?.replaceAll("\n", " ");
          return e;
        }).toList();
        updateState();
      }
    });
  }

  Future<bool> createCourse(CourseRequest request) async {
    var response = await http.post(
        Uri.parse('$baseURL/admin/courses/create'),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "x-api-key": accessToken
        },
        body: jsonEncode(request)
    );
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/admin/courses/create', method: 'POST', data: jsonEncode(request), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    final String res = response.body;
    final int statusCode = response.statusCode;
    debugPrint(json.decode(res).toString());
    if (statusCode < 200 || statusCode > 400) {
      throw HttpException(res);
    } else {
      return true;
    }
  }

  updateCourse(CourseRequest request, String courseID) async {

    var response = await http.post(
        Uri.parse('$baseURL/admin/courses/$courseID/update'),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "x-api-key": accessToken
        },
        body: jsonEncode({"update":request.toJson()})
    );
    final String res = response.body;
    final int statusCode = response.statusCode;
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/admin/courses/$courseID/update', method: 'POST', data: jsonEncode({"update":request.toJson()}), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    debugPrint(json.decode(res).toString());
    if (statusCode < 200 || statusCode > 400) {
      throw HttpException(res);
    } else {
      return true;
    }
  }

  createAudioSession(AudioRequest request) async {
    var response = await http.post(
        Uri.parse('$baseURL/admin/audios/create'),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "x-api-key": accessToken
        },
        body: jsonEncode(request)
    );
    final String res = response.body;
    final int statusCode = response.statusCode;
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/admin/audios/create', method: 'POST', data: jsonEncode(request), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    debugPrint(json.decode(res).toString());
    if (statusCode < 200 || statusCode > 400) {
      throw HttpException(res);
    } else {
      return true;
    }
  }

  updateAudioSession(AudioRequest request, String audioID) async {

    var response = await http.post(
        Uri.parse('$baseURL/admin/audios/$audioID/update'),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "x-api-key": accessToken
        },
        body: jsonEncode({"update":request.toJson()})
    );
    final String res = response.body;
    final int statusCode = response.statusCode;
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/admin/audios/$audioID/update', method: 'POST', data: jsonEncode({"update":request.toJson()}), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    debugPrint(json.decode(res).toString());
    if (statusCode < 200 || statusCode > 400) {
      throw HttpException(res);
    } else {
      return true;
    }
  }


  void generateCurlCommand(RequestOptions requestOptions) {
    // Extract relevant information from RequestOptions
    final method = requestOptions.method;
    final url = requestOptions.uri.toString();
    final headers = requestOptions.headers;
    final body = requestOptions.data;

    // Construct the cURL command
    final curlCommand = StringBuffer('curl -X $method "$url"');

    // Add headers to the cURL command
    headers.forEach((key, value) {
      curlCommand.write(' -H "$key: $value"');
    });

    // Add request body to the cURL command if it exists
    if (body != null) {
      curlCommand.write(" -d '${body.toString()}'");
    }

    // Print the final cURL command
    debugPrint(curlCommand.toString());
  }
  getProfileType() {
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/admin/profile-types', method: 'GET', headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    http.get(Uri.parse('$baseURL/admin/profile-types'), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    }).then((http.Response response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400) {
        log('Error in api');
      } else {
        log(json.decode(res).toString());
        final List parsed = await json.decode(res);
        profileTypes = parsed.map((m) {
          return ProfileType.fromJson(m);
        }).toList();
        profileTypes = profileTypes.where((element) => element.personalityType?.isNotEmpty ?? false).toList();
        updateState();
      }
    });
  }

 /* getProfileTypeId(String profileTypeId) {
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/admin/profile-types/$profileTypeId', method: 'GET', headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    http.get(Uri.parse('$baseURL/admin/profile-types/$profileTypeId'), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    }).then((http.Response response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400) {
        log('Error in api');
      } else {
        log(json.decode(res).toString());

        profileType = await json.decode(res);
        print("::::::get profiletype::::${profileType?.suggestionContent?.courses?.toList()}");
        updateState();
      }
    });
  }*/

  updateProfileType(body, String profileTypeID,context) async  {
    Utilities.showSnack(context, "Loading...");
    var response = await http.post(
        Uri.parse('$baseURL/admin/profile-types/$profileTypeID/update'),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "x-api-key": accessToken
        },
        body:
        jsonEncode({"update": body})
        );
    final String res = response.body;
    final int statusCode = response.statusCode;
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/profile-types/$profileTypeID/update', method: 'POST', data: jsonEncode({"update": body}), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
 //   debugPrint(json?.decode(res).toString());
    if (statusCode < 200 || statusCode > 400) {
      Utilities.showSnack(context, res.toString());
      throw HttpException(res);
    } else {
      Utilities.showSnack(context, "Updated Successfully...");
      return true;
    }
  }

  ProfileType? _personalityType;

  ProfileType? get personalityType => _personalityType;
  bool _isExpand = false;
  bool get isExpand => _isExpand;
  bool _isExpandPT = false;
  bool get isExpandPT => _isExpandPT;
 /* bool _isExpandDI = false;
  bool get isExpandDI => _isExpandDI;*/

  void setPersonalityType(ProfileType profileTypes) {
    _personalityType = profileTypes;
    updateState();
  }
  setFilterExpand({bool? isExpand,required BuildContext context}) {
    _isExpand = isExpand ?? !_isExpand;
    buttonWidth = context.size?.width;
    tooltipSSController.toggle();
    updateState();
  }
  setFilterExpandPT({bool? isExpandPT,required BuildContext context}) {
    _isExpandPT = isExpandPT ?? !_isExpandPT;
    buttonWidth = context.size?.width;
    tooltipPTController.toggle();
    updateState();
  }
  /*setFilterExpandDI({bool? isExpandDI,BuildContext? context}) {
    _isExpandDI = isExpandDI ?? !_isExpandDI;
    buttonWidth = context!.size?.width;
    updateState();
  }*/
  List<String> selectedItems = [];

  void toggleSelection(String audioId, ScreenType type) {
    void removeItem(List<dynamic>? items, String audioId) {
      items?.removeWhere((item) => item.id == audioId);
      updateState();
    }
    void addItem(List<dynamic>? items, String audioId, String titlePrefix) {
      if ((items?.contains(Session(id: audioId, title: "$titlePrefix $audioId")) ?? false)||(items?.contains(Course(id: audioId, name: "$titlePrefix $audioId")) ?? false)) {
        items?.clear();
        updateState();
      }
      if((type == ScreenType.suggestedEditAUDIO)||(type==ScreenType.suggestedTrackListAudio)) {
        items?.add(Session(id: audioId, title: "$titlePrefix $audioId"));
        items?.toSet().toList();
      }else{
        items?.add(Course(id: audioId, name: "$titlePrefix $audioId"));
        items?.toSet().toList();
      }
     updateState();
    }
    if ((selectedItems.contains(audioId))||(profileType?.suggestionContent?.sessions?.any((element) => element.id==audioId)??false)||
        (profileType?.suggestionContent?.courses?.any((element) => element.id==audioId)??false)||
        (tracksSuggestion?.suggestionContent?.sessions?.any((element) => element.id==audioId)??false)||
        (tracksSuggestion?.suggestionContent?.sessions?.any((element) => element.id==audioId)??false)
    ) {
      if (type == ScreenType.suggestedEditAUDIO) {
        removeItem(profileType?.suggestionContent?.sessions, audioId);
      } else {
        removeItem(profileType?.suggestionContent?.courses, audioId);
      }
      if (type == ScreenType.suggestedTrackListAudio) {
        removeItem(tracksSuggestion?.suggestionContent?.sessions, audioId);
      } else {
        removeItem(tracksSuggestion?.suggestionContent?.courses, audioId);
      }
      selectedItems.remove(audioId);
    } else {
      if (type == ScreenType.suggestedEditAUDIO) {
        addItem(profileType?.suggestionContent?.sessions, audioId, "Session");
      }
      else{
        addItem(profileType?.suggestionContent?.courses, audioId, "Course");
      }
      if (type == ScreenType.suggestedTrackListAudio) {
        addItem(tracksSuggestion?.suggestionContent?.sessions, audioId, "Session");
      }
      else{
        addItem(tracksSuggestion?.suggestionContent?.courses, audioId, "Course");
      }
      selectedItems.add(audioId);
    }

    updateState();
    print("selected :::::Item======${selectedItems.toList()}");
  }






  void selectProfileType(String selectedId) {
    if (profileType?.id == selectedId) {
      profileType = null;
    } else {
      profileType = profileTypes.firstWhere((profileType) => profileType.id == selectedId, orElse: () => ProfileType());
    }
    updateState();
  }
  void selectTracksSuggestion(String selectedId) {
    if (tracksSuggestion?.id == selectedId) {
      tracksSuggestion = null;
    } else {
      tracksSuggestion = tracksSuggestions.firstWhere((tracksSuggestion) => tracksSuggestion.id == selectedId, orElse: () => TrackSuggestions());
    }
    updateState();
  }
  getTrackSuggestion() {
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/admin/tracks-suggestions', method: 'GET', headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    http.get(Uri.parse('$baseURL/admin/tracks-suggestions'), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    }).then((http.Response response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400) {
        log('Error in api');
      } else {
        log(json.decode(res).toString());
        final List parsed = await json.decode(res);
        tracksSuggestions = parsed.map((m) {
          return TrackSuggestions.fromJson(m);
        }).toList();
        tracksSuggestions = tracksSuggestions.where((element) => element.mgpComponentDescription?.isNotEmpty??false ).toList();
        updateState();
      }
    });
  }

  getCategories() {
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/admin/categories', method: 'GET', headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    http.get(Uri.parse('$baseURL/admin/categories'), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    }).then((http.Response response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400) {
        log('Error in api');
      } else {
        log(json.decode(res).toString());
        final List parsed = await json.decode(res);
        categories = parsed.map((m) {
          return Category.fromJson(m);
        }).toList();
        categories = categories.where((element) => element.name?.isNotEmpty ?? false).toList();
        categories = categories.map((e) {
          e.name = e.name?.replaceAll("\n", " ");
          return e;
        }).toList();
        updateState();
      }
    });
  }
  updateTrackSuggestions(body, String tracksID,context) async  {
    Utilities.showSnack(context, "Loading...");
    var response = await http.post(
        Uri.parse('$baseURL/admin/tracks-suggestions/$tracksID/update'),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "x-api-key": accessToken
        },
        body: jsonEncode({"update": body}));
    final String res = response.body;
    final int statusCode = response.statusCode;
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/tracks-suggestions/$tracksID/update', method: 'POST', data: jsonEncode({"update": body}), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    //   debugPrint(json?.decode(res).toString());
    if (statusCode < 200 || statusCode > 400) {
      Utilities.showSnack(context, res.toString());
      throw HttpException(res);

    } else {
      Utilities.showSnack(context, "Updated Successfully");
      return true;
    }
  }
  createTrackSuggestions(body,context) async {
    Utilities.showSnack(context, "Loading...");
    var response = await http.post(
        Uri.parse('$baseURL/admin/tracks-suggestions/create'),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "x-api-key": accessToken
        },
        body: jsonEncode(body)
    );
    final String res = response.body;
    final int statusCode = response.statusCode;
    RequestOptions requestOptions = RequestOptions(path: '$baseURL/admin/tracks-suggestions/create', method: 'POST', data: jsonEncode(body), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      "x-api-key": accessToken
    });
    generateCurlCommand(requestOptions);
    debugPrint(json.decode(res).toString());
    if (statusCode < 200 || statusCode > 400) {
      Utilities.showSnack(context, res.toString());
      throw HttpException(res);
    } else {
      Utilities.showSnack(context, "Updated Successfully...");
      return true;
    }
  }
}


