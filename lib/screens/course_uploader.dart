import 'package:data_uploader/models/audio.dart';
import 'package:data_uploader/models/course.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';

import '../app_common_button.dart';
import '../app_constants.dart';
import '../requests/audio_request.dart';
import '../requests/course_request.dart';
import '../utilities/utilities.dart';
import 'audio_uploader.dart';
import 'audio_uploader_view_model.dart';

class CourseUploader extends StatefulWidget {
  bool isEditing;
  CourseUploader({Key? key,this.isEditing = false}) : super(key: key);

  @override
  State<CourseUploader> createState() => _CourseUploaderState();
}
class _CourseUploaderState extends State<CourseUploader> {
  // AudioUploaderViewModel viewModel = AudioUploaderViewModel();
  final _formKey = GlobalKey<FormBuilderState>();
  final _formKeyDropdown = GlobalKey<FormFieldState>();
  Course? course;
  List<String> genderOptions = ['male', 'male', 'male'];
  List<String> mentalSkills = [];
  String enteredText = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.twilightBackground,
        body: SafeArea(
          child: Center(
            child: Container(

              width: 600,


              child: SingleChildScrollView(
              padding: const EdgeInsets.all(30.0),
                child: ViewModelBuilder<UploaderViewModel>.nonReactive(
                    viewModelBuilder: () => UploaderViewModel(),
                    onViewModelReady: (UploaderViewModel model) => model.setup(),
                    builder: (BuildContext context, UploaderViewModel model,
                        Widget? child) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          FormBuilder(
                            key: _formKey,
                            autovalidateMode: AutovalidateMode.always,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ...[
                                  const SizedBox(
                                    height: 0,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: SizedBox(
                                      width: 46,
                                      height: 46,
                                      child:
                                      SvgPicture.asset('assets/back_icon.svg'),
                                    ),
                                  ),
                                  Text(
                                    'Course ${widget.isEditing ? "Update" : "Upload"}',
                                    style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                        color: Colors.white),
                                  ),
                                  const SizedBox(height: 10,),
                                  if (widget.isEditing)
                                    Consumer<UploaderViewModel>(
                                        builder: (context, value, child) {
                                          return SizedBox(
                                            height: 55,
                                            child: DropdownButtonFormField<Course>(
                                              dropdownColor: Color(0xFF1C1B1F),
                                              value: course,
                                              decoration: InputDecoration(
                                                contentPadding: const EdgeInsets.all(8),
                                                enabledBorder: OutlineInputBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(8.0),
                                                  borderSide: const BorderSide(
                                                    color: Color(0xffD5D4DC),
                                                    width: 1.0,
                                                  ),
                                                ),
                                                focusedBorder: OutlineInputBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(5.0),
                                                  borderSide: const BorderSide(
                                                    color: Color(0xffD5D4DC),
                                                    width: 1.0,
                                                  ),
                                                ),
                                              ),
                                              hint:  const Text(
                                                'Get Courses',
                                                style: TextStyle(color: Colors.white),
                                              ),
                                              //  value: ,
                                              icon: const Icon(
                                                Icons.keyboard_arrow_down_sharp,
                                                size: 32,
                                                color: Colors.white,
                                              ),
                                              elevation: 16,

                                              isExpanded: true,
                                           /*   onChanged: (Course? value) {
                                                setState(() {

                                                  course = value!;
                                                  print("courses?.name-------------------${course?.name}");
                                                  print("courses?.desc-------------------${course?.description}");
                                                  enteredText = course?.name ?? "";
                                                  _formKey.currentState?.fields['Name']?.didChange(course?.name ?? "");
                                                  _formKey.currentState?.fields['Description']?.didChange(course?.description ?? "");
                                                  _formKey.currentState?.fields['numberOfDays']?.didChange(course?.numberOfDays.toString() ?? "");
                                                  _formKey.currentState?.fields['audioImage']?.didChange(course?.headerImage?.url ?? "");

                                                  _formKey.currentState?.fields['Difficulty']?.didChange(course?.difficulty?.toString() ?? "");

                                                  if ((course?.mentalSkills != null) && course?.mentalSkills?.isNotEmpty == true) {
                                                    mentalSkills = model.skills
                                                        .where((element) => course?.mentalSkills?.contains(element) == true)
                                                        .toList();
                                                  }
                                                });
                                              }*/
                                                onChanged: (Course? value) {
                                                  setState(() {
                                                    course = value!;
                                                    mentalSkills.clear();
                                                    enteredText = course?.name ?? "";
                                                    _formKey.currentState?.fields['Name']?.didChange("dsahdjsahdjsah"?? "");
                                                    _formKey.currentState?.fields['Description']?.didChange(course?.description ?? "");
                                                    _formKey.currentState?.fields['numberOfDays']?.didChange(course?.numberOfDays.toString()?? "");
                                                    _formKey.currentState?.fields['audioImage']?.didChange(course?.headerImage?.url ?? "");
                                                    _formKey.currentState?.fields['mentalSkills']?.didChange(course?.mentalSkills  as  List<String> ?? []);
                                                    mentalSkills.addAll(course?.mentalSkills as Iterable<String>);
                                                    _formKey.currentState?.fields['Difficulty']?.didChange(model.difficulty[course?.difficulty ?? 0]);

                                                  });
                                                },

                                              items: model.courses
                                                  .map<DropdownMenuItem<Course>>(
                                                      (Course value) {
                                                    return DropdownMenuItem<Course>(
                                                      value: value,
                                                      child: Text(
                                                        value.name.toString(),
                                                        style: TextStyle(color: Colors.white),
                                                      ),
                                                    );
                                                  }).toList(),
                                            ),
                                          );
                                        }),
                                  CustomTextField(name: 'Name', label: 'Name', onChanged: (va){
                                  },
                                    initialValue: course?.name,),
                                  // const CustomTextField(name: 'ID',label: 'ID'),
                                  const CustomTextField(
                                      name: 'Description', label: 'Description'),
                                  const CustomTextField(
                                      name: 'audioImage', label: 'Image'),
                                  const CustomTextField(
                                      name: 'numberOfDays',
                                      label: 'Number of days',
                                      keyboardType: TextInputType.number),
                                  SizedBox(
                                    width: 326,
                                    child: MultiSelectDialogField(
                                      selectedItemsTextStyle: const TextStyle(color: Colors.white),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        border: Border.all(color: Colors.white),
                                      ),
                                      backgroundColor: const Color(0xff0B1F23),
                                      key: _formKeyDropdown,

                                      initialValue: List<String>.from(mentalSkills),

                                      itemsTextStyle: const TextStyle(color: Colors.white),
                                      buttonText: const Text('Mental Skills',
                                          style: TextStyle(color: Colors.white)),
                                      buttonIcon: const Icon(
                                        Icons.arrow_drop_down,
                                        color: Color(0xff0B1F23),
                                      ),

                                      dialogHeight: genderOptions.length * 50,
                                      title: const Text('Mental Skills',style: TextStyle(color: Colors.white),),
                                      items: model.skills
                                          .map((e) => MultiSelectItem(e, e))
                                          .toList(),

                                      onConfirm: (values) {
                                        mentalSkills = values as List<String>;
                                      },

                                    ),
                                  ),
                                  FormBuilderRadioGroup(
                                    activeColor: Colors.white,
                                    decoration: const InputDecoration(
                                      labelText: 'Difficulty Level',
                                      labelStyle: TextStyle(color: Colors.white),
                                      border: InputBorder.none,
                                    ),
                                    initialValue: course?.difficulty,
                                    name: 'Difficulty',
                                    options: model.difficulty
                                        .map((lang) =>
                                        FormBuilderFieldOption(value: lang))
                                        .toList(growable: false),
                                  ),
                                ].expand(
                                      (widget) => [
                                    widget,
                                    const SizedBox(
                                      height: 15,
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: AppCommonButton(
                                  width: 362,
                                  height: 56,
                                  radius: 8,
                                  title: (widget.isEditing?'Publish Changes ':'Publish '),
                                  onPressed: () async {
                                    _formKey.currentState?.save();
                                    if (!(_formKey.currentState?.validate() ??
                                        false)) {
                                      return;
                                    }
                                    debugPrint(_formKey.currentState?.value.toString());
                                    debugPrint(_formKey.currentState?.value['Title'].toString());
                                    Utilities.showSnack(
                                        context, 'Creating course...');
                                    try {
                                      CourseRequest request = CourseRequest();
                                      request.name =
                                      _formKey.currentState?.value['Name'];
                                      request.description = _formKey.currentState?.value['Description'];
                                      request.mentalSkills = mentalSkills;
                                      request.difficulty = model.difficulty.indexOf(_formKey.currentState?.value['Difficulty']);
                                      request.numberOfDays = int.parse(_formKey.currentState?.value['numberOfDays']);
                                      Images audioImage = Images();
                                      audioImage.url = _formKey.currentState?.value['audioImage'];
                                      request.headerImage = audioImage;
                                      if (widget.isEditing && course != null) {
                                        await model.updateCourse(
                                            request,
                                            course?.id ?? '');
                                      } else {
                                        await model.createCourse(request);
                                      }

                                      Utilities.showSnack(context, 'Course created successfully');
                                      Navigator.of(context).pop();
                                    } catch (error) {
                                      Utilities.showSnack(context, error.toString());
                                    }
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      );
                    }),
              ),
            ),
          ),
        ));
  }
}
