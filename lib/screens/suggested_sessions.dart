import 'package:data_uploader/app_constants.dart';
import 'package:data_uploader/models/audio.dart';
import 'package:data_uploader/models/profile_type.dart';
import 'package:data_uploader/models/track_suggestions.dart';
import 'package:data_uploader/screens/audio_uploader_view_model.dart';
import 'package:data_uploader/widgets/dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';
import '../app_common_button.dart';
import '../models/course.dart';
import '../utilities/enum_type.dart';
import '../widgets/expanded_widget/expanded_widget.dart';

class SuggestionList extends StatefulWidget {
  bool isEditing;
 final ScreenType type;
  SuggestionList({Key? key, this.isEditing = false, required this.type,}) : super(key: key);
  @override
  State<SuggestionList> createState() => _SuggestionListState();
}

class _SuggestionListState extends State<SuggestionList> {
  final _formKey = GlobalKey<FormBuilderState>();
  Course? course;
  ProfileType? profileType;
  TrackSuggestions? tracksSuggestion;
  Audio? audio;
  bool isChecked = false;
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: AppColors.twilightBackground,
        body: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: 600,
            color: const Color(0xff0B1F23),
            child: SingleChildScrollView(
              child: ViewModelBuilder<UploaderViewModel>.nonReactive(
                viewModelBuilder: () => UploaderViewModel(),
                onViewModelReady: (UploaderViewModel model) => model.setup(),
                builder: (BuildContext context, UploaderViewModel model,
                    Widget? child) {

               //   print("::::::::::::::::model:::::::${model.profileType[0].personalityType}");
  return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    FormBuilder(
                      key: _formKey,
                      autovalidateMode: AutovalidateMode.always,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ...[
                              const SizedBox(height: 50,),
                              BasicButton(
                                  padding: const EdgeInsets.all(3),
                                  child: SvgPicture.asset(
                                    AppAssets.backIcon,
                                    height: 46,
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  }),
                              const SizedBox(height: 40,),
                              Text('Suggested ${widget.isEditing ? "List Edit" : "Track List"}'
                                  ' ${widget.type==ScreenType.suggestedEditAUDIO||widget.type==ScreenType.suggestedTrackListAudio?"Audio":"Course"}',
                                style: const TextStyle(fontSize: 16,fontWeight: FontWeight.w700,color: Colors.white),),
                              const SizedBox(height: 30,),
                              Consumer<UploaderViewModel>(
                                builder: (context, value, child) {
                                  return Column(
                                    children: [
                                      CustomDropDown(
                                        width: value.buttonWidth??MediaQuery.of(context).size.width,
                                       tooltipController: value.tooltipPTController,
                                        onTap: () {
                                          value.setFilterExpandPT(context: context);
                                        },
                                        isOpen: value.isExpandPT,
                                        trailing: BasicButton(
                                          onPressed: () {
                                            value.setFilterExpandPT(context: context);
                                          },
                                          child: RotatableWidget(
                                            rotate: value.isExpandPT,
                                            child: const Icon(
                                              Icons.keyboard_arrow_down,
                                              size: 35,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        title:widget.type==ScreenType.suggestedEditAUDIO||widget.type==ScreenType.suggestedEditCOURSES?
                                        value.profileType?.personalityType:value.tracksSuggestion?.mgpComponentDescription,
                                        child:widget.type == ScreenType.suggestedEditAUDIO || widget.type == ScreenType.suggestedEditCOURSES
                                            ? _buildProfileTypeListView(value)
                                            : _buildTracksSuggestionListView(value),
                                      ),
                                      AnimatedContainer(duration: const Duration(milliseconds: 200),height: !value.isExpandPT?10:235,),
                                    ],
                                  )
                                  ;
                                },
                              ),


                            ]
                          ],
                        ),
                      ),
                    ),
                  //  const SizedBox(height: 20,),
                    Visibility(
                      visible: false,
                      child: AppCommonButton(
                          width: 362,
                          height: 52,
                          radius: 8,
                          title: 'Get List',
                          onPressed: () async {
                        /*  await model.getProfileTypeId(model.profileType?.id??"");*/
                          }

                      ),
                    ),
                    const SizedBox(height: 20,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Consumer<UploaderViewModel>(
                        builder: (context, value, child) {
                          return Column(
                            children: [
                              CustomDropDown(
                                width: value.buttonWidth??MediaQuery.of(context).size.width,
                                tooltipController: value.tooltipSSController,
                                isOpen: value.isExpand,
                                onTap: () => value.setFilterExpand(context: context),
                                trailing: BasicButton(
                                  onPressed: () {
                                    value.setFilterExpand(context: context);
                                  },
                                  child: RotatableWidget(
                                      rotate: value.isExpand,
                                      child: const Icon(
                                        Icons.keyboard_arrow_down,
                                        size: 35,
                                        color: Colors.white,
                                      )),
                                ),
                                title: "Current Audio (call all session IDs)",
                                child:value.audios.isNotEmpty||value.courses.isNotEmpty? ListView.builder(
                                  itemCount: widget.type == ScreenType.suggestedEditAUDIO||widget.type == ScreenType.suggestedTrackListAudio ? value.audios.length : value.courses.length,
                                  itemBuilder: (context, index) {
                                    if (widget.type == ScreenType.suggestedEditAUDIO||widget.type == ScreenType.suggestedTrackListAudio) {
                                      audio = value.audios[index];
                                      return buildItem(value,audio?.sId, audio?.title,index);
                                    } else {
                                      course = value.courses[index];
                                      return buildItem(value,course?.id, course?.name,index);
                                    }
                                  },
                                ):const Center(child: CircularProgressIndicator(),)
                              ),
                              AnimatedContainer(duration: Duration(milliseconds: 200),height: !value.isExpand?20:265,),
                            ],
                          );
                        },
                      ),

                    ),


                    AppCommonButton(
                        width: 362,
                        height: 52,
                        radius: 8,
                        title: 'Update',
                      onPressed: () async {

                        if (widget.type == ScreenType.suggestedEditAUDIO||widget.type==ScreenType.suggestedEditCOURSES) {
                          if (widget.type == ScreenType.suggestedEditAUDIO) {
                          Map<String, dynamic> body = {
                                  "suggestionContent": {
                                    "sessions": model.profileType?.suggestionContent?.sessions
                                            ?.map((session) =>
                                                session.id.toString()).toSet()
                                            .toList() ??
                                        [],
                                  }
                              };
                          await  model.updateProfileType(body, model.profileType?.id ?? "",context);
                        }else{
                          Map<String, dynamic> body = {

                                  "suggestionContent": {
                                    "courses":model.profileType?.suggestionContent?.courses
                                            ?.map((course) =>
                                        course.id.toString()).toSet()
                                            .toList() ??
                                        [],
                                  }

                              };

                          await   model.updateProfileType(body, model.profileType?.id ?? "",context);
                        }
                        }else{
                          if (widget.type == ScreenType.suggestedTrackListAudio) {
                            Map<String, dynamic> body = {
                                    "suggestionContent": {
                                      "sessions":model.tracksSuggestion?.suggestionContent?.sessions?.map((session) => session.id.toString()).toSet().toList() ?? [],
                                    }

                                };


                            if(model.tracksSuggestion?.suggestionContent?.sessions?.isNotEmpty??false){
                              await   model.updateTrackSuggestions(body, model.tracksSuggestion?.id ?? "",context);
                            }else{
                              await   model.createTrackSuggestions(body,context);
                            }

                          }else{
                            Map<String, dynamic> body = {
                                    "suggestionContent": {
                                      "courses": model.tracksSuggestion?.suggestionContent?.courses
                                              ?.map((course) =>
                                          course.id.toString()).toSet()
                                              .toList() ??
                                          [],
                                    }

                                };
                            if(model.tracksSuggestion?.suggestionContent?.courses?.isNotEmpty??false){
                             await model.updateTrackSuggestions(body, model.tracksSuggestion?.id ?? "",context);
                            }else{
                              await  model.createTrackSuggestions(body,context);
                            }
                          }
                        }

                      },

                      // async {
                      //   _formKey.currentState?.save();
                      //   if (!(_formKey.currentState?.validate() ?? false)) {
                      //     return;
                      //   }
                      //   debugPrint(_formKey.currentState?.value.toString());
                      //   Utilities.showSnack(context, 'Creating course...');
                      //   try {
                      //     AudioRequest request = AudioRequest();
                      //     request.title = _formKey.currentState?.value['title'];
                      //     request.subtitle = _formKey.currentState?.value['subtitle'];
                      //
                      //     request.difficulty = _formKey.currentState?.value['Difficulty'];
                      //     request.courseID = courseIds;
                      //     request.locationID = locationIds.map((e) => e.sId ?? '').toList();
                      //     request.augmentedAudios = augmentedAudioIds.map((e) => e.sId ?? '').toList();
                      //     request.similarSessions = similarSessionModels;
                      //     request.courseOrder = int.parse(_formKey.currentState?.value['courseOrder']);
                      //     request.rating = double.parse(_formKey.currentState?.value['rating']);
                      //
                      //     ImageModel audioImage = ImageModel();
                      //     audioImage.url = _formKey.currentState?.value['audioImage'];
                      //     request.image = audioImage;
                      //
                      //     Description description = Description();
                      //     description.long = _formKey.currentState?.value['description'];
                      //     request.description = description;
                      //
                      //     Content content = Content();
                      //     content.url = _formKey.currentState?.value['url'] ?? "";
                      //     if (_formKey.currentState?.value['lengthMins'] is int) {
                      //       content.lengthMins = int.parse(_formKey.currentState?.value['lengthMins']);
                      //     } else {
                      //       content.lengthMins = 0;
                      //     }
                      //     request.content = [content];
                      //
                      //     Attributes attributes = Attributes();
                      //     if (_formKey.currentState?.value['numberOfBalls'] is int) {
                      //       attributes.balls = int.parse(_formKey.currentState?.value['numberOfBalls'] ?? '');
                      //     }
                      //     if (mentalSkills.contains('Calm')) {
                      //       attributes.calm = 1;
                      //     }
                      //     if (mentalSkills.contains('Confidence')) {
                      //       attributes.confidence = 1;
                      //     }
                      //     if (mentalSkills.contains('Focus')) {
                      //       attributes.focus = 1;
                      //     }
                      //     request.attributes = attributes;
                      //
                      //     VoicedBy voicedBy = VoicedBy();
                      //     voicedBy.name = _formKey.currentState?.value['voicedByName'];
                      //     ImageModel imageModel = ImageModel();
                      //     imageModel.url = _formKey.currentState?.value['voicedByImage'];
                      //     voicedBy.image = imageModel;
                      //     request.voicedBy = voicedBy;
                      //
                      //     request.isPremium = _formKey.currentState?.value['isPremium'];
                      //     request.isTopRated = _formKey.currentState?.value['isTopRated'];
                      //     request.isAugmented = _formKey.currentState?.value['IsAugmented'];
                      //     debugPrint(json.encode(request.toJson()).toString());
                      //     if (widget.isEditing && selectedAudio != null) {
                      //       await model.updateAudioSession(request, selectedAudio?.sId ?? '');
                      //     }
                      //     else {
                      //       await model.createAudioSession(request);
                      //     }
                      //     Utilities.showSnack(context, 'Audio session created successfully');
                      //     Navigator.of(context).pop();
                      //   } catch (error) {
                      //     Utilities.showSnack(context, error.toString());
                      //   }
                      // },
                    ),
                  ]
                ),
              );
  },
)
            ),
          ),
        ));
  }
  Widget buildItem(UploaderViewModel model, String? id, String? name, int index) {
    return Consumer<UploaderViewModel>(
      builder: (context, provider, child) {
        bool isSelectedByProvider = provider.selectedItems.contains(id);
        bool isSelectedBySession = false;
        if (widget.type == ScreenType.suggestedEditAUDIO || widget.type == ScreenType.suggestedEditCOURSES) {
          if (widget.type == ScreenType.suggestedEditAUDIO) {
            isSelectedBySession = model.profileType?.suggestionContent?.sessions?.any((session) => session.id == id) ?? false;
          } else {
            isSelectedBySession = model.profileType?.suggestionContent?.courses?.any((courses) => courses.id == id) ?? false;
          }
        } else {
          if (widget.type == ScreenType.suggestedTrackListAudio) {
            isSelectedBySession = model.tracksSuggestion?.suggestionContent?.sessions?.any((session) => session.id == id) ?? false;
          } else {
            isSelectedBySession = model.tracksSuggestion?.suggestionContent?.courses?.any((courses) => courses.id == id) ?? false;
          }
        }
        bool isSelected = isSelectedByProvider || isSelectedBySession;
        return ListTile(
          onTap: () {
            provider.toggleSelection(id ?? "", widget.type);
          },
          title: Text(name ?? "", style: const TextStyle(color: Colors.white)),
          trailing: Checkbox(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(2.0),
            ),
            fillColor: MaterialStateProperty.all(Color(0xFFD0BCFF)),
            checkColor: AppColors.check,
            value: isSelected,
            onChanged: (isChecked) {
              provider.toggleSelection(id ?? "", widget.type);
            },
          ),
        );
      },
    );
  }






  Widget _buildListItem({
    required UploaderViewModel value,
    required Function() onTap,
    required String title,
    required dynamic item,
  }) {
    return Column(
      children: [
        ListTile(
          onTap: onTap,

          title: Text(
            title,
            style: const TextStyle(color: Colors.white),
          ),
         /* trailing: Checkbox(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(2.0),
            ),
            fillColor: MaterialStateProperty.all(const Color(0xFFD0BCFF)),
            checkColor: AppColors.check,
            value: item is ProfileType
                ? value.profileType?.id == item.id
                : item is TrackSuggestions
                ? value.tracksSuggestion?.id == item.id
                : false,
            onChanged: (isChecked) {
              if (item is ProfileType) {
                value.selectProfileType(item?.id ?? "");
                value.profileType?.id = item?.id;
              } else if (item is TrackSuggestions) {
                value.selectTracksSuggestion(item?.id ?? "");
                value.tracksSuggestion?.id = item?.id;
              }
            },
          ),*/
        ),
      ],
    );
  }
  Widget _buildTracksSuggestionListView(UploaderViewModel value) {
    return value.tracksSuggestions.isNotEmpty?ListView.builder(
      itemCount: value.tracksSuggestions.length,
      itemBuilder: (context, index) {
        final tracksSuggestions = value.tracksSuggestions[index];
        return _buildListItem(
          value: value,
          item: tracksSuggestions,
          onTap: () => value.selectTracksSuggestion(tracksSuggestions?.id ?? ""),
          title: "${tracksSuggestions?.mgpComponentDescription}",
        );
      },
    ):const Center(child: CircularProgressIndicator(),);
  }
  Widget _buildProfileTypeListView(UploaderViewModel value) {
    return value.profileTypes.isNotEmpty?ListView.builder(
      itemCount: value.profileTypes.length,
      itemBuilder: (context, index) {
        final profileType = value.profileTypes[index];
        return _buildListItem(
          value: value,
          item: profileType,
          onTap: () => value.selectProfileType(profileType?.id ?? ""),
          title: "${profileType?.personalityType}",
        );
      },
    ):const Center(child: CircularProgressIndicator(),);
  }
}


