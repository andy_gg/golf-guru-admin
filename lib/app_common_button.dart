import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'app_constants.dart';

class AppCommonButton extends StatelessWidget {
  final String title;
  final double? height;
  final double? width;
  final VoidCallback? onPressed;
  final double? fontSize;
  final double radius;
  final Color color;
  final Color textColor;
  final double elevation;
  final bool? isLoading;
  final double borderWidth;
  final Color borderColor;
  final Image? icon;
  const AppCommonButton({
    Key? key,
    required this.title,
    this.onPressed,
    this.height,
    this.width,
    this.fontSize = 16,
    this.radius = 8,
    this.color = AppColors.mainLightGreen,
    this.elevation = 2.0,
    this.isLoading,
    this.borderColor = AppColors.offWhite,
    this.borderWidth = 0.5,
    this.textColor = Colors.white,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return (isLoading ?? false)
        ? const Center(
            child: CircularProgressIndicator(
              strokeWidth: 2,
            ),
          )
        :
        // SizedBox(
        //   height: height,
        //   width: width,
        //   child: ElevatedButton(
        //     onPressed: () {
        //       onPressed?.call();
        //     },
        //     style: ElevatedButton.styleFrom(
        //         elevation: elevation,
        //          primary: color,
        //         side: BorderSide(width: borderWidth, color: borderColor,),
        //         shape: RoundedRectangleBorder(
        //             borderRadius: BorderRadius.circular(radius),
        //         )
        //     ),
        //     child:  Row(
        //       mainAxisAlignment: MainAxisAlignment.center,
        //       children: [
        //         Text(title,
        //           style: TextStyle(color: textColor,fontSize: fontSize,fontWeight: FontWeight.w800),
        //         ),
        //         if (icon != null)...[
        //           const SizedBox(width: 5,),
        //           icon!
        //         ]
        //       ],
        //     ),
        //   ),
        // );
        Container(
            width: width,
            height: height,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(radius),
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: <Color>[
                  const Color(0xff216C57),
                  const Color(0xff216C57).withOpacity(0),
                ],
              ),
              border: Border.all(color: borderColor, width: borderWidth),
            ),
            child: MaterialButton(
              onPressed: () {
                onPressed?.call();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                        color: textColor,
                        fontSize: fontSize,
                        fontWeight: FontWeight.w500),
                  ),
                  if (icon != null) ...[
                    const SizedBox(
                      width: 5,
                    ),
                    icon!
                  ]
                ],
              ),
            ),
          );
  }
}

customAudioCard({

  required BuildContext context,
  required String img,
  required String imgUrl,
  required isPremium,
  required String title,
  required String subTitle,
  required VoidCallback onTap,
}) {
  return InkWell(
    onTap: onTap,
    child: Container(
      padding: const EdgeInsets.all(16),
       height: 232,
      width: 195,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          /*gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              const Color(0xff216C57),
              const Color(0xff143926).withOpacity(0),
            ],
          ),*/
          border: Border.all(color: const Color(0xff216C57))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Stack(children: [
            Container(
              width: 160,
              height: 99,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image:
                    DecorationImage(image: NetworkImage(img), fit: BoxFit.cover,),
              ),
            ),
            Visibility(
              visible: isPremium,
              child: Positioned(
                bottom: 10,
                left: 10,
                child: Container(
                  width: 31,
                  height: 31,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: SvgPicture.asset("assets/Lock.svg"),
                ),
              ),
            ),
          ]),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 6),
            child: Text(
              title,
              maxLines: 3,
              style: const TextStyle(
                  color: Color(0xffFFFFFF),
                  fontWeight: FontWeight.w600,
                  fontSize: 16),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                height: 22,
                width: 23,
                decoration:  BoxDecoration(
                    shape: BoxShape.circle, color: Color(0xffD9D9D9),
                    image:
                    DecorationImage(image: NetworkImage(imgUrl), fit: BoxFit.cover,)
                ),
              ),
              Container(
                alignment: Alignment.center,
                height: 25,
                width: 102,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: const Color(0xffF5F4E1)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 16,
                      height: 16,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: SvgPicture.asset("assets/Icon1.svg"),
                    ),
                     Text(
                      '$subTitle mins',
                      style: TextStyle(
                          fontSize: 12,
                          color: Color(0xffFFFFFF),
                          fontWeight: FontWeight.w400),
                    ),
                    Container(
                      width: 17,
                      height: 17,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: SvgPicture.asset("assets/Driving.svg"),
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    ),
  );
}

// class customAudioCard extends StatefulWidget {
//   final String label;
//   final String name;
//   final String img;
//
//   final TextInputType keyboardType;
//   final String? initialValue;
//
//   const customAudioCard({
//     Key? key,
//     required BuildContext context,
//     required this.label,
//     required this.img,
//     this.keyboardType = TextInputType.name,
//     this.initialValue, required this.name,
//   }) : super(key: key);
//
//   @override
//   State<customAudioCard> createState() => _CustomTextFieldState();
// }

// class _CustomTextFieldState extends State<customAudioCard> {
//   FocusNode myFocusNode = FocusNode();
//
//   @override
//   Widget build(BuildContext context) {
//     return InkWell(
//       child: Container(
//         padding: const EdgeInsets.all(16),
//         height: 232,
//         width: 195,
//         decoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(8),
//             gradient: LinearGradient(
//               begin: Alignment.topLeft,
//               end: Alignment.bottomRight,
//               colors: [
//                 const Color(0xff216C57),
//                 const Color(0xff143926).withOpacity(0),
//               ],
//             ),
//             border: Border.all(color: const Color(0xff216C57))),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: [
//             Stack(children: [
//               Container(
//                 width: 160,
//                 height: 99,
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(8),
//                   // border: Border.all(color: const Color(0xff81BCBB)),
//                   image:
//                   DecorationImage(image: AssetImage(widget.img), fit: BoxFit.fill
//                   ),
//                 ),
//               ),
//               Positioned(
//                 bottom: 10,
//                 left: 10,
//                 child: Container(
//                   width: 31,
//                   height: 31,
//                   decoration: const BoxDecoration(
//                     shape: BoxShape.circle,
//                   ),
//                   child: SvgPicture.asset("assets/Lock.svg"),
//                 ),
//               ),
//             ]),
//             FormBuilderTextField(
//               decoration: InputDecoration(
//                 hintText: widget.label,
//                 contentPadding: const EdgeInsets.all(8),
//                 hintStyle: const TextStyle(color: Colors.white,fontSize: 16,fontWeight: FontWeight.w500),
//                 // floatingLabelStyle: const TextStyle(color: AppColors.mainLightGreen),
//                 border: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(8.0),
//                   borderSide: const BorderSide(
//                     color: Color(0xffD5D4DC),
//                     width: 1.0,
//                   ),
//                 ),
//                 enabledBorder: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(8.0),
//                   borderSide: const BorderSide(
//                     color: Color(0xffD5D4DC),
//                     width: 1.0,
//                   ),
//                 ),
//                 focusedBorder: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(8.0),
//                   borderSide: const BorderSide(
//                     color: Color(0xffD5D4DC),
//                     width: 1.0,
//                   ),
//                 ),
//                 focusedErrorBorder: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(8.0),
//                   borderSide: const BorderSide(
//                     color: Color(0xffD5D4DC),
//                     width: 1.0,
//                   ),
//                 ),
//                 errorBorder: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(8.0),
//                   borderSide: const BorderSide(
//                     color: Color(0xffD5D4DC),
//                     width: 1.0,
//                   ),
//                 ),
//               ),
//               initialValue: widget.initialValue ?? "",
//               style: const TextStyle(
//                   color: Color(0xffFFFFFF),
//                   fontWeight: FontWeight.w600,
//                   fontSize: 16), name: widget.name,
//             ),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Container(
//                   height: 22,
//                   width: 23,
//                   decoration: const BoxDecoration(
//                       shape: BoxShape.circle, color: Color(0xffD9D9D9)),
//                 ),
//                 Container(
//                   alignment: Alignment.center,
//                   height: 25,
//                   width: 102,
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(8),
//                     border: Border.all(color: const Color(0xffF5F4E1)),
//                   ),
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                     children: [
//                       Container(
//                         width: 16,
//                         height: 16,
//                         decoration: const BoxDecoration(
//                           shape: BoxShape.circle,
//                         ),
//                         child: SvgPicture.asset("assets/Icon1.svg"),
//                       ),
//                       const Text(
//                         '20 mins',
//                         style: TextStyle(
//                             fontSize: 12,
//                             color: Color(0xffFFFFFF),
//                             fontWeight: FontWeight.w400),
//                       ),
//                       Container(
//                         width: 17,
//                         height: 17,
//                         decoration: const BoxDecoration(
//                           shape: BoxShape.circle,
//                         ),
//                         child: SvgPicture.asset("assets/Driving.svg"),
//                       ),
//                     ],
//                   ),
//                 )
//               ],
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

customNavigatorButton({
  required String text,
  required VoidCallback onTap,
  required Color color,
  required Color colortxt,
  required BorderRadius borderRadius,
}) {
  return GestureDetector(
    onTap: onTap,
    child: Container(
      height: 48,
      width: 346,
      decoration: BoxDecoration(
        borderRadius: borderRadius,
        color: color,
      ),
      child: Center(
          child: Text(
        text,
        style: TextStyle(
            color: colortxt, fontSize: 16, fontWeight: FontWeight.w700),
      )),
    ),
  );
}
class BasicButton extends StatelessWidget {
  final Widget child;
  final Function()? onPressed;
  final EdgeInsets padding;
  final bool isDisable;
  const BasicButton(
      {Key? key,
        required this.child,
        required this.onPressed,
        this.padding = EdgeInsets.zero,
        this.isDisable = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: isDisable ? 0.7 : 1,
      child: CupertinoButton(
          padding: padding,
          minSize: 0,
          child: child,
          onPressed: isDisable ? null : onPressed),
    );
  }
}