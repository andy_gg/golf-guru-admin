import '../models/audio.dart';

class AudioRequest {
  List<int>? lId;
  int? adaloId;
  Attributes? attributes;
  AudioRequirements? audioRequirements;
  List<String>? categoryIDs;
  List<Content>? content;
  List<String>? courseID;
  int? courseOrder;
  Description? description;
  String? difficulty;
  ImageModel? image;
  bool? isPremium;
  bool? isTopRated;
  bool? isAugmented;
  int? order;
  num? rating;
  List<int>? subCategoryIDs;
  String? subtitle;
  String? title;
  VoicedBy? voicedBy;
  List<String>? augmentedAudios;
  List<String>? locationID;
  List<SimilarSessions>? similarSessions;
  AudioRequest(
      {this.lId,
      this.adaloId,
      this.attributes,
      this.audioRequirements,
      this.categoryIDs,
      this.content,
      this.courseID,
      this.courseOrder,
      this.description,
      this.difficulty,
      this.image,
      this.isPremium,
      this.isTopRated,
      this.locationID,
      this.order,
      this.rating,
      this.subCategoryIDs,
      this.subtitle,
      this.title,
      this.voicedBy,
      this.augmentedAudios,
      this.isAugmented,this.similarSessions
      });

  AudioRequest.fromJson(Map<String, dynamic> json) {
    lId = json['_id'].cast<int>();
    adaloId = json['adaloId'];
    attributes = json['attributes'] != null
        ? Attributes.fromJson(json['attributes'])
        : null;
    audioRequirements = json['audioRequirements'] != null
        ? AudioRequirements.fromJson(json['audioRequirements'])
        : null;
    categoryIDs = json['categoryIDs'].cast<int>();
    if (json['content'] != null) {
      content = <Content>[];
      json['content'].forEach((v) {
        content!.add(Content.fromJson(v));
      });
    }
    courseID = json['courseID'].cast<int>();
    courseOrder = json['courseOrder'];
    description = json['description'] != null
        ? Description.fromJson(json['description'])
        : null;
    difficulty = json['difficulty'];
    image = json['image'] != null ? ImageModel.fromJson(json['image']) : null;
    isPremium = json['isPremium'];
    isTopRated = json['isTopRated'];
    isAugmented = json['isAugmented'];
    locationID = json['locationIDs']?.cast<String>();
    order = json['order'];
    rating = json['rating'];
    subCategoryIDs = json['subCategoryIDs'].cast<int>();
    subtitle = json['subtitle'];
    title = json['title'];
    voicedBy = json['voicedBy'] != null
        ? VoicedBy.fromJson(json['voicedBy'])
        : null;
    augmentedAudios =  json['augmentedAudios']?.cast<String>();
    if (json['similarSessions'] != null) {
      similarSessions = <SimilarSessions>[];
      json['similarSessions'].forEach((v) { similarSessions!.add(SimilarSessions.fromJson(v)); });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = lId;
    data['adaloId'] = adaloId;
    if (attributes != null) {
      data['attributes'] = attributes!.toJson();
    }
    if (audioRequirements != null) {
      data['audioRequirements'] = audioRequirements!.toJson();
    }
    data['categoryIDs'] = categoryIDs;
    if (content != null) {
      data['content'] = content!.map((v) => v.toJson()).toList();
    }
    data['courseID'] = courseID;
    data['courseOrder'] = courseOrder;
    if (description != null) {
      data['description'] = description!.toJson();
    }
    data['difficulty'] = difficulty;
    if (image != null) {
      data['image'] = image!.toJson();
    }
    data['isPremium'] = isPremium;
    data['isTopRated'] = isTopRated;
    data['isAugmented'] = isAugmented;
    data['locationIDs'] = locationID;
    data['order'] = order;
    data['rating'] = rating;
    data['subCategoryIDs'] = subCategoryIDs;
    data['subtitle'] = subtitle;
    data['title'] = title;
    if (voicedBy != null) {
      data['voicedBy'] = voicedBy!.toJson();
    }
    data['augmentedAudios'] = augmentedAudios;
    if (similarSessions != null) {
      data['similarSessions'] = similarSessions!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}


class AudioRequirements {
  int? numberOfBalls;

  AudioRequirements({this.numberOfBalls});

  AudioRequirements.fromJson(Map<String, dynamic> json) {
    numberOfBalls = json['numberOfBalls'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['numberOfBalls'] = numberOfBalls;
    return data;
  }
}

class Content {
  String? gender;
  String? language;
  int? lengthMins;
  String? previewUrl;
  String? url;

  Content(
      {this.gender, this.language, this.lengthMins, this.previewUrl, this.url});

  Content.fromJson(Map<String, dynamic> json) {
    gender = json['gender'];
    language = json['language'];
    lengthMins = json['lengthMins'];
    previewUrl = json['previewUrl'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['gender'] = gender;
    data['language'] = language;
    data['lengthMins'] = lengthMins;
    data['previewUrl'] = previewUrl;
    data['url'] = url;
    return data;
  }
}

class Description {
  String? long;
  String? short;

  Description({this.long, this.short});

  Description.fromJson(Map<String, dynamic> json) {
    long = json['long'];
    short = json['short'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['long'] = long;
    data['short'] = short;
    return data;
  }
}

class ImageModel {
  String? filename;
  int? height;
  int? size;
  String? url;
  int? width;

  ImageModel({this.filename, this.height, this.size, this.url, this.width});

  ImageModel.fromJson(Map<String, dynamic> json) {
    filename = json['filename'];
    height = json['height'];
    size = json['size'];
    url = json['url'];
    width = json['width'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['filename'] = filename;
    data['height'] = height;
    data['size'] = size;
    data['url'] = url;
    data['width'] = width;
    return data;
  }
}

class VoicedBy {
  ImageModel? image;
  String? name;

  VoicedBy({this.image, this.name});

  VoicedBy.fromJson(Map<String, dynamic> json) {
    image = json['image'] != null ? ImageModel.fromJson(json['image']) : null;
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (image != null) {
      data['image'] = image!.toJson();
    }
    data['name'] = name;
    return data;
  }
}

class SimilarSessions {
  String? id;
  int? order;
  String? title;

  SimilarSessions({this.id, this.order,this.title});

  SimilarSessions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    order = json['order'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['order'] = order;
    return data;
  }
}