import 'package:data_uploader/models/audio.dart';



class CourseRequest {
  String? id;
  String? name;
  String? description;
  List<String>? mentalSkills;
  int? difficulty;
  int? numberOfDays;
  Images? headerImage;
  dynamic locationIDs;
  dynamic categoryIDs;
  int? createdAt;
  int? updatedAt;
  CourseRequest(
      { this.id,
        this.name,
        this.description,
        this.mentalSkills,
        this.difficulty,
        this.numberOfDays,
        this.headerImage,
        this.locationIDs,
        this.categoryIDs,
        this.createdAt,
        this.updatedAt,});

  factory CourseRequest.fromJson(Map<String, dynamic> json) => CourseRequest(
    id: json["_id"],
    name: json["name"],
    description: json["description"],
    mentalSkills: json["mentalSkills"] == null ? [] : List<String>.from(json["mentalSkills"]!.map((x) => x)),
    difficulty: json["difficulty"],
    numberOfDays: json["numberOfDays"],
    headerImage: json["headerImage"] == null ? null : Images.fromJson(json["headerImage"]),
    locationIDs: json["locationIDs"],
    categoryIDs: json["categoryIDs"],
    createdAt: json["createdAt"],
    updatedAt: json["updatedAt"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "description": description,
    "mentalSkills": mentalSkills == null ? [] : List<dynamic>.from(mentalSkills!.map((x) => x)),
    "difficulty": difficulty,
    "numberOfDays": numberOfDays,
    "headerImage": headerImage?.toJson(),
    "locationIDs": locationIDs,
    "categoryIDs": categoryIDs,
    "createdAt": createdAt,
    "updatedAt": updatedAt,
  };
}