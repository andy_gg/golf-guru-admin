// ignore_for_file: constant_identifier_names

enum ViewState {INIT, LOADING, COMPLETED, ERROR, EMPTY}
enum ConfirmAction { CANCEL, ACCEPT }
enum TimelineCommentOption { EDIT, DELETE, REPORT }
enum SearchType {WOMEN, MEN, NOSPECIFIED, TRAVELERS, LOCALS, FRIENDS}