
import 'dart:ui';

import 'package:flutter/material.dart';

class AppConstants {
  AppConstants._();

  static const String defaultFont = 'Inter';

  static const String ddMMMYYY = 'dd MMM yyyy';

  static const String aboutUsURL = "";
  static const String revenueCatAppleKey = "appl_nkkmKxrymnligXuZypfaLUtkckH";
  static const String revenueCatGoogleKey = "goog_XRuMiKeldaQvvlmzJEsMCydUATx";
  static const String workWithUsURL = "";
}

class AppAssets {
  AppAssets._();

  static const String icCalendarSVG = 'assets/svgs/ic_calendar.svg';
  static const String icCheckCircleSVG = 'assets/svgs/ic_check_circle.svg';
  static const String icClockSVG = 'assets/svgs/ic_clock.svg';
  static const String icHomeTabSVG = 'assets/svgs/ic_home_tab.svg';
  static const String icNoteTabSVG = 'assets/svgs/ic_note_tab.svg';
  static const String icProfileTabSVG = 'assets/svgs/ic_profile_tab.svg';
  static const String icSearchTabSVG = 'assets/svgs/ic_search_tab.svg';
  static const String icProfileHomeSVG = 'assets/svgs/ic_profile_home.svg';
  static const String icLockSVG = 'assets/svgs/ic_lock.svg';
  static const String icSettingSVG = 'assets/svgs/ic_setting.svg';
  static const String icStarSVG = 'assets/svgs/ic_star.svg';
  static const String icLongArrowSVG = 'assets/svgs/ic_long_arrow.svg';
  static const String icCourseSkill1 = 'assets/svgs/ic_course_skill_1.svg';
  static const String icCourseSkill2 = 'assets/svgs/ic_course_skill_2.svg';
  static const String icCourseSkill3 = 'assets/svgs/ic_course_skill_3.svg';
  static const String icDraftNote = 'assets/svgs/ic_draft_note.svg';
  static const String icHeadphone = 'assets/svgs/ic_headphone.svg';
  static const String icGolfClubs = 'assets/svgs/ic_golf_clubs.svg';
  static const String backIcon = 'assets/back_icon.svg';
}

class AppColors {
  AppColors._();
  static const Color jetStream = Color(0xFFB2D7CB);
  static const Color darkSlateGray = Color(0xFF28564E);
  static const Color gunmetal = Color(0xFF2C3633);
  static const Color mediumJungleGreen = Color(0xFF1B352E);
  static const Color darkSpringGreen = Color(0xFF15604B);
  static const Color amazon = Color(0xFF386559);
  static const Color illuminatingEmerald = Color(0xFF3E9175);
  static const Color msuGreen = Color(0xFF194739);
  static const Color greenSheen = Color(0xFF75AC99);
  static const Color charlestonGreen = Color(0xFF282828);
  static const Color etonBlue = Color(0xFF83BEAA);
  static const Color japaneseIndigo = Color(0xFF26443C);
  static const Color brass = Color(0xFFB4A63F);
  static const Color peach = Color(0xFFDEBF87);
  static const Color quickSilver = Color(0xFFA4A4A4);
  static const Color raisinBlack = Color(0xFF202225);
  static const Color darkSilver = Color(0xFF6C706F);
  static const Color silverSand = Color(0xFFC4C4C4);
  static const Color ghostWhite = Color(0xFFF8F9FA);
  static const Color alabaster = Color(0xFFEEF3E4);
  static const Color davyGrey = Color(0xFF495057);
  static const Color antiFlashWhite = Color(0xFFF3F3F3);
  static const Color turquoiseGreen = Color(0xFF95C9B7);
  static const Color brunswickGreen = Color(0xFF1D543E);
  static const Color brunswickGreen2 = Color(0xFF24483E);
  static const Color seaGreen = Color(0xFF2D7F62);
  static const Color morningBlue = Color(0xFF809F95);
  static const Color darkJungleGreen = Color(0xFF1D231F);
  static const Color kombuGreen = Color(0xFF384824);
  static const Color opal = Color(0xFF9FC3B7);
  static const Color phthaloGreen = Color(0xFF0E2C28);
  static const Color twilightBackground = Color(0xFF0B1F23);
  static const Color check = Color(0xFF381E72);

  static const MaterialColor mainOrange = MaterialColor(
    0xFFf29222,
    <int, Color>{
      50: Color(0xFFf29222),
      100: Color(0xFFf29222),
      200: Color(0xFFf29222),
      300: Color(0xFFf29222),
      400: Color(0xFFf29222),
      500: Color(0xFFf29222),
      600: Color(0xFFf29222),
      700: Color(0xFFf29222),
      800: Color(0xFFf29222),
      900: Color(0xFFf29222),
    },
  );

  static const Color mainLightGray = Color.fromRGBO(196, 196, 196, 1);
  static const Color mainDarkGray = Color.fromRGBO(162, 162, 162, 1);
  static const Color mainLightGreen = Color.fromRGBO(29, 84, 62, 1);
  static const Color mainDarkGreen = Color.fromRGBO(45, 54, 51, 1);
  static const Color calm = Color.fromRGBO(27, 245, 180, 1);
  static const Color confidence = Color.fromRGBO(190, 243, 77, 1);
  static const Color focus = Color.fromRGBO(34, 175, 255, 1);
  static const Color offWhite = Color.fromRGBO(245, 240, 225, 1);
  static const Color paymentOffWhite = Color.fromRGBO(253, 243, 239, 1);
  static const Color sessionBorder = Color.fromRGBO(178, 215, 203, 1);
  static const Color progress1 = Color.fromRGBO(120, 255, 209, 1);
  static const Color progress2 = Color.fromRGBO(76, 158, 130, 1);
  static const Color progress3 = Color.fromRGBO(53, 100, 84, 1);
  static const Color sheetBackground = Color.fromRGBO(29, 37, 34, 1);
  static const Color placeholder = Color.fromRGBO(113, 113, 113, 1);
  static const Color shareGradientTop = Color.fromRGBO(48, 60, 56, 1);
  static const Color shareGradientBottom = Color.fromRGBO(123, 142, 137, 1);
  static const Color selectedPackage = Color.fromRGBO(158, 199, 185, 1);
}

class AppTextStyles {
  AppTextStyles._();
  static const TextStyle normal = TextStyle(
      fontFamily: AppConstants.defaultFont,
      color: Colors.white,
      fontWeight: FontWeight.normal);
  static const TextStyle light = TextStyle(
      fontFamily: AppConstants.defaultFont,
      color: Colors.white,
      fontWeight: FontWeight.w200);
  static const TextStyle medium = TextStyle(
      fontFamily: AppConstants.defaultFont,
      color: Colors.white,
      fontWeight: FontWeight.w500);
  static const TextStyle semiBold = TextStyle(
      fontFamily: AppConstants.defaultFont,
      color: Colors.white,
      fontWeight: FontWeight.w600);
  static const TextStyle bold = TextStyle(
      fontFamily: AppConstants.defaultFont,
      color: Colors.white,
      fontWeight: FontWeight.bold);
}
