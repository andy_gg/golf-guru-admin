import 'package:flutter/material.dart';

class ExpandableWidget extends StatefulWidget  {
  final Widget child;
  final bool expand;
  final Axis axis;

  const ExpandableWidget(
      {Key? key,
        this.expand = false,
        required this.child,
        this.axis = Axis.vertical})
      : super(key: key);

  @override
  _ExpandableWidgetState createState() => _ExpandableWidgetState();
}

class _ExpandableWidgetState extends State<ExpandableWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController expandController;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    initAnimations();
    handleAnimations();
  }

  @override
  void didUpdateWidget(ExpandableWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    handleAnimations();
  }

  @override
  void dispose() {
    expandController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizeTransition(
        axisAlignment: 1.0,
        axis: widget.axis,
        sizeFactor: animation,
        child: widget.child);
  }

  void initAnimations() {
    expandController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 600));
    animation = CurvedAnimation(
      parent: expandController,
      curve: Curves.easeInOutBack,
    );
  }

  void handleAnimations() {
    if (widget.expand) {
      expandController.forward();
    } else {
      expandController.reverse();
    }
  }
}


class RotatableWidget extends StatefulWidget {
  final Widget child;
  final bool rotate;
  final double end;

  const RotatableWidget(
      {Key? key, this.rotate = false, required this.child, this.end = 0.5})
      : assert(end <= 1, 'end value should between 0-1'),
        super(key: key);

  @override
  _RotatableWidgetState createState() => _RotatableWidgetState();
}

class _RotatableWidgetState extends State<RotatableWidget>
    with SingleTickerProviderStateMixin {
    late AnimationController rotationController;
    late Animation<double> animation;
    late Animation<double> turns;

  @override
  void initState() {
    super.initState();
    initAnimations();
    handleAnimations();
  }

  @override
  void didUpdateWidget(RotatableWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    handleAnimations();
  }

  @override
  void dispose() {
    rotationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(turns: turns, child: widget.child);
  }

  /// Setting animations up
  void initAnimations() {
    rotationController =
        AnimationController(vsync: this, duration: const Duration(milliseconds: 600));
    animation = CurvedAnimation(
      parent: rotationController,
      curve: Curves.easeInOutBack,
    );
    turns = Tween(begin: 0.0, end: widget.end).animate(animation);
  }

  /// Handling expand status of the widget
  void handleAnimations() {
    if (widget.rotate) {
      rotationController.forward();
    } else {
      rotationController.reverse();
    }
  }
}
