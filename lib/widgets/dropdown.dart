import 'package:data_uploader/widgets/custom_dropdown.dart';
import 'package:data_uploader/widgets/expanded_widget/expanded_widget.dart';
import 'package:flutter/material.dart';

class CustomDropDown extends StatefulWidget {
  const CustomDropDown({
    Key? key,
    this.child,
    this.title,
    required this.isOpen,
    this.trailing, required this.tooltipController, this.onTap, required this.width,
  }) : super(key: key);

  final Widget? child;
  final String? title;
  final bool isOpen;
  final Widget? trailing;
  final Function()? onTap;
  final double width;
  final OverlayPortalController tooltipController;

  @override
  State<StatefulWidget> createState() => CustomDropDownState();
}

class CustomDropDownState extends State<CustomDropDown> {
  final OverlayPortalController _tooltipController = OverlayPortalController();
  final _link = LayerLink();
  double? _buttonWidth;

  @override
  Widget build(BuildContext context) {
    return CompositedTransformTarget(
      link: _link,
      child: OverlayPortal(

        controller: widget.tooltipController,
        overlayChildBuilder: (BuildContext context) {
          return CompositedTransformFollower(
            link: _link,
            targetAnchor: Alignment.bottomLeft,
            child: Align(
              alignment: AlignmentDirectional.topStart,
              child: ExpandableWidget(
                expand: widget.isOpen,
                child: MenuWidget(width: widget.width, child: widget.child),
              ),
            ),
          );
        },
        child: ButtonWidget(
          width: widget.width,
          onTap: widget.onTap,
          child: Padding(
            padding: const EdgeInsets.all(12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    widget.title ?? " ",
                    textAlign: TextAlign.start,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                widget.trailing ?? SizedBox.shrink(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

