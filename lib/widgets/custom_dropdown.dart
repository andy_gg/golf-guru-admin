import 'package:data_uploader/app_constants.dart';
import 'package:flutter/material.dart';


class ButtonWidget extends StatelessWidget {
  const ButtonWidget({Key? key,

    this.height = 48,
    this.width,
    this.onTap,
    this.child,

  }) : super(key: key);

  final double? height;
  final double? width;

  final VoidCallback? onTap;

  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: Material(
        color: AppColors.twilightBackground,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
          side: const BorderSide(color: Colors.white),
        ),
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(8),
          child: child ?? const SizedBox(),
        ),
      ),
    );
  }
}

class MenuWidget extends StatelessWidget {
  const MenuWidget({

    this.width, this.height,  this.child,
  });

  final double? width;
  final double? height;
  final Widget? child;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? 327,
      height: height??250,
      margin: EdgeInsets.symmetric(horizontal:0),
      decoration: ShapeDecoration(

        color: Color(0xFF1C1B1F),
        shape: RoundedRectangleBorder(
          side: const BorderSide(
            width: 1,
            color: Colors.white,
          ),
          borderRadius: BorderRadius.circular(0),
        ),
        shadows: const [
          BoxShadow(
            color: Color(0x11000000),
            blurRadius: 32,
            offset: Offset(0, 20),
            spreadRadius: -8,
          ),
        ],
      ),
      child: child,
    );
  }
}