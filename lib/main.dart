import 'package:data_uploader/utilities/enum_type.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:form_builder_validators/localization/l10n.dart';


import 'app_common_button.dart';
import 'screens/audio_uploader.dart';
import 'screens/course_uploader.dart';
import 'screens/suggested_sessions.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        //buttonTheme: ButtonThemeData(colorScheme: ColorScheme.light()),
        ///colorScheme: ColorScheme.dark(),
        // Define the default brightness and colors.
        // brightness: Brightness.light,
        primaryColor: Colors.white,

        radioTheme: const RadioThemeData(fillColor: MaterialStatePropertyAll(Colors.white)),
        // Define the default font family.
        //fontFamily: 'Georgia',
        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: const TextTheme(
          bodyMedium: TextStyle(
              fontSize: 14.0, fontFamily: 'Hind', color: Colors.white),
          labelLarge: TextStyle(
              fontSize: 14.0, fontFamily: 'Hind', color: Colors.black),
        ),
      ),
      //home: const Dummy(),
      home: const MyHomePage(),
      supportedLocales: const [
        Locale('bn'),
        Locale('de'),
        Locale('en'),
        Locale('es'),
        Locale('fr'),
        Locale('it'),
        Locale('lo'),
        Locale('uk'),
      ],
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        FormBuilderLocalizations.delegate,
      ],
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              customNavigatorButton(
                  text: 'Upload Session',
                  onTap: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => AudioUploader()));
                  },
                  color: const Color(0xff264837),
                  colortxt: Colors.white,
                  borderRadius: BorderRadius.circular(30)),
              const SizedBox(
                height: 30,
              ),
              customNavigatorButton(
                  text: 'Upload Course',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>  CourseUploader()));
                  },
                  color: const Color(0xff264837),
                  colortxt: Colors.white,
                  borderRadius: BorderRadius.circular(30)),
              const SizedBox(
                height: 30,
              ),
              customNavigatorButton(
                  text: 'Update Session',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => AudioUploader(
                              isEditing: true,
                            )));
                  },
                  color: const Color(0xffD9D9D9),
                  colortxt: Colors.black,
                  borderRadius: BorderRadius.circular(16)),
              SizedBox(height: 30,),
              customNavigatorButton(
                  text: 'Update Course',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => CourseUploader(
                          isEditing: true,
                        )));
                  },
                  color: const Color(0xffD9D9D9),
                  colortxt: Colors.black,
                  borderRadius: BorderRadius.circular(16)),
              const SizedBox(
                height: 30,
              ),
              customNavigatorButton(
                  text: 'Suggestions Personality Audio',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => SuggestionList(isEditing: true,type: ScreenType.suggestedEditAUDIO,)));
                  },
                  color: const Color(0xffD9D9D9),
                  colortxt: Colors.black,
                  borderRadius: BorderRadius.circular(16)),
              const SizedBox(
                height: 30,
              ),
              customNavigatorButton(
                  text: 'Suggestions Personality Courses',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => SuggestionList(isEditing: true,type: ScreenType.suggestedEditCOURSES,)));
                  },
                  color: const Color(0xffD9D9D9),
                  colortxt: Colors.black,
                  borderRadius: BorderRadius.circular(16)),
              const SizedBox(
                height: 30,
              ),
              customNavigatorButton(
                  text: 'Suggestions Track Audio',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => SuggestionList(type: ScreenType.suggestedTrackListAudio,)));
                  },
                  color: const Color(0xffD9D9D9),
                  colortxt: Colors.black,
                  borderRadius: BorderRadius.circular(16)),
              const SizedBox(
                height: 30,
              ),
              customNavigatorButton(
                  text: 'Suggestions Track Courses',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => SuggestionList(type: ScreenType.suggestedTrackListCourses,)));
                  },
                  color: const Color(0xffD9D9D9),
                  colortxt: Colors.black,
                  borderRadius: BorderRadius.circular(16)),
            ],
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
