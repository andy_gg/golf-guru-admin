import 'package:flutter/material.dart';


import 'view_state.dart';

class BaseModel extends ChangeNotifier {
  ViewState _state = ViewState.INIT;
  ViewState get state => _state;

  void setState(ViewState newState) {
    _state = newState;
    notifyListeners();
  }

  void updateState() {
    notifyListeners();
  }
}
